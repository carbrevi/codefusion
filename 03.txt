o Objective
  Solve NASA CRM setup with high-order methods efficiently, outperforming 2nd-order FVM in resolution and computational time.

o Features
  - High-order CPR scheme (2nd-4th order)
    * Consider the discretization of 2D convective/diffusion model equations
    * Consider the discretization of Euler, NS and RANS with SA model
  - h-adaptivity, output-based anisotropic mesh
	* Consider the work by M. Yano from MIT
    * Consider the BAMG (http://people.sc.fsu.edu/~jburkardt/examples/bamg/bamg.html) mesh generator
  - High-order mesh elements with embeded CAD interpreter
    * Consider the work of Persson and Peraire with curved meshes
	* Consider the IGES input format (C-based reader library)
    * Consider the Ayam (www.ayam3d.org) library
  - MPL limiter scheme
    * Check paper of Park et. at.
  - Implicit scheme
    * Consider the Paralution (www.paralution.com) and ViennaCL (http://viennacl.sourceforge.net) libraries
  - GPGPU capable
	* Consider the OpenMP 4.0 Offload feature
	* Consider the OpenACC 2.0 standard
  - High-Order solution visualization tool
    * work independent of the spatial discretization method

o Coding Specifics
  - C language
    * Consider musl libc
	* Consider libdhcore (https://github.com/septag/libdhcore) utilities library
  - Simple build system
    * Plain Makefiles
  - Parallel capabilities
    * MPI, OpenMP, GPU (OpenMP Offload)
	* Parallel-IO based on HDF5
  - LUA bind for scripting support
    * Specific BC setting
  - Linux support
    * gcc, llvm, intel compilers support
  - Windows support
    * mingw-w64, msmpi

o GUI
  - C++ language
  - Consider the CEF interface (https://code.google.com/p/chromiumembedded and cefbuilds.com)
  - Check features of HOOPS library (www.hoops3d.com)
  - Check out imediate mode GUIs
