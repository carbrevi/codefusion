import os,sys,platform,shutil
import common
import packages

# ---------------------------------------------------------

def init():

  # if the list is not empty
  if common.TPL:

    print()
    print("---------------------------------------------------------")
    print("This script will compile and install the following")
    print("Third Party Libraries (TPL):" )
    print()
    print( common.TPL )
    print()
    print( ("into \"%s\" directory.") %( common.installDir ) )
    print("Please, make sure you have proper permissions.")
    print()
    print("Log files for configure, make and install for each TPL will be recorded")
    print( ("into \"%s\" directory.") %( common.buildDir ) )
    print()
    print( ("The make process will use \"%s\" processors.") %( str(common.NPROCS) ) )
    print("---------------------------------------------------------")
    print()

    if common.python_ver == 2:
      isok = raw_input("Are you OK with these settings/edit? [Y/n/e]: ")
    elif common.python_ver == 3:
      isok = input("Are you OK with these settings/edit? [Y/n/e]: ")
    print()

    if ( isok == "e" ):

      # for now, the user can edit the number of process
      # used by make and the compiler environment variables
      if common.python_ver == 2:
        NPROCS = raw_input("Enter number of jobs for make: ")
      elif common.python_ver == 3:
        NPROCS = input("Enter number of jobs for make: ")
      print()
      common.NPROCS = NPROCS

      # Go over the initial message screen
      isok = ""
      common.clearScreen()
      init()

    quitNow = False
    if ( (isok) or (isok == "n") ): quitNow = True
    if ( (not isok) or (isok == "Y") or (isok == "y") ): quitNow = False

    if ( quitNow ):
      print("exiting now...\n")
      # finish script execution.
      common.terminate(0)

  # if the list is empty
  else:

    print("There are no TPLs to build.")
    print("exiting now...\n")
    common.terminate(0)

  return


# ---------------------------------------------------------

def cleanPreviousBuild():

  # If a previous installation directory is found, ask the user
  # if he/she wants to remove it or not. If there is a directory
  # present, we can greatly speedup things by rebuilding only
  # the missing TPLs.

  if os.path.exists( common.installDir ):

    if common.python_ver == 2:
      rmDir = raw_input("A previous installation directory was found. Remove? [y/N]: ")
    elif common.python_ver == 3:
      rmDir = input("A previous installation directory was found. Remove? [y/N]: ")
    print()

    removeDir = False
    if ( (rmDir) or (rmDir == "y") ): removeDir = True
    if ( (not rmDir) or (rmDir == "N") or (rmDir == "n") ): removeDir = False

    if removeDir:

      # ok, lets remove previous dir and build TPLs again

      shutil.rmtree( common.installDir )
      os.makedirs( common.installDir )

    else:

      # We must remove the already built TPL in installDir from the
      # TPL list to build now.

      # list only folder in the installDir path
      builtTPL = ""
      for x in os.listdir(common.installDir):
        if os.path.isfile(x): pass
        else: builtTPL += x + ";"

      # convert strings to list and remove empty items on list
      builtTPL = builtTPL.split(';')
      builtTPL = [x for x in builtTPL if x]

      for item in builtTPL:

        for TPL in common.packages:

          if ( item == TPL.lower() ):
            setattr(common,(('%s_install_dir')%(TPL.lower())),common.installDir+"/"+item)

        # we need to populate the common variables of the TPLs
        # that were found on installDir.
        packages.set_packages_common_vars( item )

        if item in common.TPL:
          # remove the already built TPL from the list, if it is there.
          common.TPL.remove(item)

      # Go over the initial message screen
      common.clearScreen()
      init()

  else:

    # no previous installations were found.
    os.makedirs( common.installDir )

  # Previous build directories are always erased.
  if os.path.exists( common.buildDir ):
    shutil.rmtree( common.buildDir )
    os.makedirs( common.buildDir )

# ---------------------------------------------------------

def cleanBuild():

  print("Apparently everything went well...")


  if common.python_ver == 2:
    rmok = raw_input("Remove the TPL build directory? [y/N]: ")
  elif common.python_ver == 3:
    rmok = input("Remove the TPL build directory? [y/N]: ")
  print()

  rmBuildDir = False
  if ( (rmok) or (rmok == "y") ): rmBuildDir = True
  if ( (not rmok) or (rmok == "N") or (rmok == "n") ): rmBuildDir = False
  if rmBuildDir: shutil.rmtree( common.buildDir )

  return

# ---------------------------------------------------------