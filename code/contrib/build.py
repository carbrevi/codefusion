#!/usr/bin/python -B
#
# Invoking python with the B switch turns off bytecode
# generation in the source tree.
#
# This script builds all 3rd-party libraries and installs
# them to the location provided by the user.

from __future__ import print_function

import os, sys, platform, tarfile, glob, shutil, time, signal
from os.path import basename
import multiprocessing
from subprocess import call

import common, packages, parseinput, dependencies, messages

# function to terminate script if CTRL+C is pressed
def sigint_handler(signum, frame):
  common.endTime = time.time()
  common.printTimer()
  common.terminate(1)

signal.signal(signal.SIGINT, sigint_handler)


# ---------------------------------------------------------
# read packages configuration file

common.set_packages_definitions()

# ---------------------------------------------------------
# Get data from input file created by CMake.

parseinput.read()

common.clearScreen()

# Script initialization
common.init()

# Set and verify the required environment variables
dependencies.checkEnvVar()

# Parse the requested TPL list and check dependencies
dependencies.checkDeps()

# ---------------------------------------------------------
# Inform and ask user about the work to be done

messages.init()

messages.cleanPreviousBuild()

# ---------------------------------------------------------
# Build selected packages

# reset timer to reflect actual compute time
common.startTime = time.time()

for item in common.TPL:

  common.assert_is_ok = True

  # build each TPL using its own script
  packages.build( item )

  common.endTime = time.time()

  if not common.assert_is_ok:
    print( ("Error occured for: \"%s\" TPL") %(item) )
    print("Check the appropriate log file for more details.")
    common.terminate(1)
    break

# Ask the user to remove the build directory
if common.assert_is_ok:
  messages.cleanBuild()


# ---------------------------------------------------------
# Script termination

if common.assert_is_ok:
  common.printTimer()
  common.terminate(0)
else:
  common.printTimer()
  common.terminate(1)