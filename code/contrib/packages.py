import os
import sys
import common
import importlib

# Add all directories in packages/ to the script scope
directories = [x[0] for x in os.walk("./packages")]

# Import module from packages with build rules
for item in directories:
  sys.path.insert(0, item)

moduleNames = []
for item in directories:
  for files in os.listdir(item):
    if files.endswith(".py"):
      moduleNames.append(os.path.splitext(files)[0])

for item in moduleNames:
  exec('import %s'%item)

# ---------------------------------------------------------

def build( TPL ):

  for item in common.packages:

    if ( TPL.lower().find(item.lower()) >= 0 ):
      eval((("m_%s")%item.lower())).build()

  print()

  return

# ---------------------------------------------------------

def set_packages_common_vars( TPL ):

  # The common.package_install_dir variable should have been set from
  # a source outside the build loop. For example, if we are using a
  # previous install directory.

  for item in common.packages:

    if ( TPL.lower().find(item.lower()) >= 0 ):
      eval((("m_%s")%item.lower())).define_common_vars()

  return

# ---------------------------------------------------------