import os, sys, platform, tarfile, time
import multiprocessing
from subprocess import call

# ---------------------------------------------------------
# common object public members

global installDir
global platform_name
global platform_arch
global build_type
global build_libs_as
global build_with_mpi
global build_GUI
global build_tests
global real_type

global TPL_DIR
global TPL
global MAKEFLAGS
global COMPILER
global NPROCS
global baseDir
global buildDir

global lib_flag_autotools
global lib_flag_cmake
global assert_is_ok
global python_ver

global MPI_CXX_COMPILER
global MPI_C_COMPILER
global MPI_Fortran_COMPILER
global MPI_CXX_INCLUDE_PATH
global MPI_C_INCLUDE_PATH
global MPI_Fortran_INCLUDE_PATH
global MPI_C_LIBRARIES
global MPI_CXX_LIBRARIES
global MPI_Fortran_LIBRARIES
global MPIEXEC

global PTHREADS_INCLUDE_DIR
global PTHREADS_LIBRARY

global OpenGL_INCLUDE_DIRS
global OpenGL_LIBRARIES
global OpenGL_gl_LIBRARY
global OpenGL_glu_LIBRARY

global PIXZ_EXECUTABLE

global packages
global packages_system
global packages_numeric
global packages_gui

packages         = []
packages_system  = []
packages_numeric = []
packages_gui     = []

global depsList
depsList = {}

# ---------------------------------------------------------
# Grab packages data from configuration file

def set_packages_definitions():

  f = open("packages.inp","r")
  for line in f:

    # grab packages names
    if line.startswith("package_system"):
      key, value = line.split("=")
      packages_system.append( value.strip() )
      packages.append( value.strip() )
    if line.startswith("package_numeric"):
      key, value = line.split("=")
      packages_numeric.append( value.strip() )
      packages.append( value.strip() )
    if line.startswith("package_gui"):
      key, value = line.split("=")
      packages_gui.append( value.strip() )
      packages.append( value.strip() )

    # grab packages dependencies
    if line.startswith("deps_"):
      key, value = line.split("=")
      depsList[key.strip()] = value.strip()

  f.close()

# ---------------------------------------------------------

def clearScreen():

  # common function to clear screen

  # detect OS
  OS = platform.system()

  if OS == "Linux": os.system('clear')
  if OS == "Windows": os.system('cls')

  return

# ---------------------------------------------------------

def init():

  global TPL
  global MAKEFLAGS
  global COMPILER
  global NPROCS
  global baseDir
  global buildDir

  global platform_name
  global platform_arch

  global build_GUI

  global build_type
  global build_type_autotools
  global build_type_cmake

  global build_libs_as
  global lib_flag_autotools
  global lib_flag_cmake

  global assert_is_ok
  global python_ver
  global startTime
  global endTime

  # general script initialization

  # initialize timers
  startTime = time.time()
  endTime = time.time()

  # get python version
  if sys.version_info < (3, 0):
    python_ver = 2
  elif sys.version_info >= (3, 0):
    python_ver = 3

  # get number of system processors for parallel make compilation (shared memory).
  NPROCS = multiprocessing.cpu_count()

  # get current directory
  baseDir = os.getcwd()

  # building directory
  buildDir = installDir + "/_build"

  # make flags
  MAKEFLAGS = "VERBOSE=1 --jobs=" + str(NPROCS)


  # Set compiler suite based on user input
  set_compiler_vars("default")


  # set common build flags

  if ( build_libs_as.lower() == "shared" ):
    build_libs_as = "shared"
    lib_flag_autotools = "--enable-shared --disable-static"
    lib_flag_cmake = "-DBUILD_SHARED_LIBS:BOOL=ON"
  if ( build_libs_as.lower() == "static" ):
    build_libs_as = "static"
    lib_flag_autotools = "--enable-static --disable-shared"
    lib_flag_cmake = "-DBUILD_SHARED_LIBS:BOOL=OFF"

  if ( build_type.lower() == "debug" ):
    build_type = "debug"
    build_type_cmake = "-DCMAKE_BUILD_TYPE:STRING=Release"
  if ( build_type.lower() == "release" ):
    build_type = "release"
    build_type_cmake = "-DCMAKE_BUILD_TYPE:STRING=Debug"

  if ( platform_name.lower() == "linux" ):
    platform_name = "linux"
  if ( platform_name.lower() == "windows" ):
    platform_name = "windows"

  if ( platform_arch.lower() == "x32" ):
    platform_arch = "x32"
  if ( platform_arch.lower() == "x64" ):
    platform_arch = "x64"

  if ( build_GUI.lower() == "true" ):
    build_gui = "true"
  if ( build_GUI.lower() == "false" ):
    build_gui = "false"

  # Initialize global assertion flag
  assert_is_ok = True

  return

# ---------------------------------------------------------

def set_compiler_vars( suite ):

  # this function allows one to dynamically change the compiler
  # for a specific TPL during the build process. The m_package
  # build functions should set the desired compiler suite and
  # is also responsible to revert it to the default values.

  global compiler
  global compiler_default

  global CC
  global CXX
  global FC
  global CC_default
  global CXX_default
  global FC_default

  global MPI_CC
  global MPI_CXX
  global MPI_F90
  global MPI_CC_default
  global MPI_CXX_default
  global MPI_F90_default

  if ( suite == "default" ):
    if ( compiler_default.lower().find("gcc"  ) >= 0 ): compiler = "gcc"
    if ( compiler_default.lower().find("intel") >= 0 ): compiler = "intel"
    CC = CC_default
    CXX = CXX_default
    MPI_CC = CC_default
    MPI_CXX = CXX_default
    MPI_F90 = FC_default
  elif ( suite == "gcc" ):
    compiler = "gcc"
    CC  = "gcc"
    CXX = "g++"
    FC  = "gfortran"
  elif ( suite == "intel" ):
    compiler = "intel"
    CC  = "icc"
    CXX = "icpc"
    FC  = "ifort"

  # Set MPI environment variables
  if ( compiler == "gcc" ):
    os.environ['MPI_CC']  = "gcc"
    os.environ['MPI_CXX'] = "g++"
    os.environ['MPI_F90'] = "gfortran"
    # set OpenMPI compilers
    os.environ['OMPI_CC']  = "gcc"
    os.environ['OMPI_CXX'] = "g++"
    os.environ['OMPI_FC']  = "gfortran"
    # set Intel MPI compilers
    os.environ['I_MPI_CC']  = "gcc"
    os.environ['I_MPI_CXX'] = "g++"
    os.environ['I_MPI_F90'] = "gfortran"
  elif ( compiler == "intel" ):
    os.environ['MPI_CC']  = "icc"
    os.environ['MPI_CXX'] = "icpc"
    os.environ['MPI_F90'] = "ifort"
    # set OpenMPI compilers
    os.environ['OMPI_CC']  = "icc"
    os.environ['OMPI_CXX'] = "icpc"
    os.environ['OMPI_FC']  = "ifort"
    # set Intel MPI compilers
    os.environ['I_MPI_CC']  = "icc"
    os.environ['I_MPI_CXX'] = "icpc"
    os.environ['I_MPI_F90'] = "ifort"

  # export compiler variables to the system.
  os.environ['CC']  = CC
  os.environ['CXX'] = CXX
  os.environ['FC']  = FC
  os.environ['MPI_CC']  = MPI_CC
  os.environ['MPI_CXX'] = MPI_CXX
  os.environ['MPI_F90'] = MPI_F90
  # OpenMPI compilers
  os.environ['OMPI_CC']  = CC
  os.environ['OMPI_CXX'] = CXX
  os.environ['OMPI_FC']  = FC
  # Intel MPI compilers
  os.environ['I_MPI_CC']  = CC
  os.environ['I_MPI_CXX'] = CXX
  os.environ['I_MPI_F90'] = FC

  return

# ---------------------------------------------------------

def extract_package( name, pkgfile ):

  print(name + ": extracting")

  if not os.path.exists( buildDir ):
    os.makedirs( buildDir )

  if ( pkgfile.lower().endswith(('.tar.gz', '.tar', '.bz2')) ):
    tar = tarfile.open( pkgfile )
    for data in tar:
      tar.extract( data, path=buildDir )

  if ( pkgfile.lower().endswith('.tpxz') ):
    command = "%s -p %s -x < %s | tar x -C %s" %(PIXZ_EXECUTABLE, NPROCS, pkgfile, buildDir)
    cmdstatus = call( command, shell=True )
    if ( cmdstatus != 0 ):
      print("extraction failed.")
      terminate(1)

  return

# ---------------------------------------------------------

def make_package( name ):

  print(name + ": make")

  logfile = open( buildDir + "/" + name + "_make.log", 'w')

  command = "make %s" %( MAKEFLAGS )

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): assert_is_ok = False

  return

# ---------------------------------------------------------

def install_package( name ):

  print(name + ": install")

  logfile = open( buildDir + "/" + name + "_install.log", 'w')

  command = "make install"

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): assert_is_ok = False

  return

# ---------------------------------------------------------

def define_lib_name( lib ):

  preffix = "lib"

  if ( build_libs_as == "shared" and platform_name == "linux" ):
    suffix = ".so"
  if ( build_libs_as == "shared" and platform_name == "windows" ):
    suffix = ".dll.a"

  if ( build_libs_as == "static" and platform_name == "linux" ):
    suffix = ".a"
  if ( build_libs_as == "static" and platform_name == "windows" ):
    suffix = ".a"

  lib_fullname = preffix + lib + suffix

  return lib_fullname

# ---------------------------------------------------------

def printTimer():

  # print total time used
  elapsed = endTime - startTime

  if ( elapsed < 60 ):
    print("\nSystem elapsed time [sec] = %f\n" %elapsed )
  else:
    print("\nSystem elapsed time [min] = %f\n" %(elapsed/60.0) )

# ---------------------------------------------------------

def terminate( signal ):

  # common function to terminate the script
  # signal =  0 normal termination
  # signal != 0 abnormal termination

  print()

  sys.exit(signal)

# ---------------------------------------------------------