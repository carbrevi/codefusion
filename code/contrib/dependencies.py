import sys, os
import common

# ---------------------------------------------------------

def checkEnvVar():

  # Before build the required TPL, we must make sure that
  # the compilers environment variables are defined properly.

  os.environ['CC']  = common.CC
  os.environ['CXX'] = common.CXX
  os.environ['FC']  = common.FC
  os.environ['MPI_CC']  = common.MPI_CC
  os.environ['MPI_CXX'] = common.MPI_CXX
  os.environ['MPI_F90'] = common.MPI_F90

  CC  = os.environ['CC']
  CXX = os.environ['CXX']
  FC  = os.environ['FC']
  MPI_CC  = os.environ['MPI_CC']
  MPI_CXX = os.environ['MPI_CXX']
  MPI_F90 = os.environ['MPI_F90']
  # variables for OpenMPI
  os.environ['OMPI_CC']  = common.CC
  os.environ['OMPI_CXX'] = common.CXX
  os.environ['OMPI_FC']  = common.FC
  # variables for Intel MPI
  os.environ['I_MPI_CC']  = common.CC
  os.environ['I_MPI_CXX'] = common.CXX
  os.environ['I_MPI_FC']  = common.FC

  problem = False
  if ( not CC  ): problem = True
  if ( not CXX ): problem = True
  if ( not FC  ): problem = True
  if ( not MPI_CC  ): problem = True
  if ( not MPI_CXX ): problem = True
  if ( not MPI_F90 ): problem = True

  if not problem:
    print("The following compiler environment will be considered:")
    if ( CC  ): print( "CC  = %s" %CC  )
    if ( CXX ): print( "CXX = %s" %CXX )
    if ( FC  ): print( "FC  = %s" %FC  )
    if ( MPI_CC  ): print( "MPI_CC  = %s" %MPI_CC   )
    if ( MPI_CXX ): print( "MPI_CXX = %s" %MPI_CXX  )
    if ( MPI_F90 ): print( "MPI_F90 = %s" %MPI_F90  )

  if problem:
    print()
    print("Your compiler environment variables are not setup properly.")
    print("Please, verify the value of CC, CXX and FC before proceeding.")
    print("Also check the parallel compiler values in MPI_CC, MPI_CXX and MPI_f90.")
    print("Exiting now...")
    print()
    common.terminate(1)

# ---------------------------------------------------------

def checkDeps():

  # split TPLs string from CMake into a python list
  TPL = common.TPL.split(';')

  # list of required dependencies
  deps = ""

  # List each TPL full tree dependency.
  for item in TPL:
    key = "deps_" + item
    if key in common.depsList:
      deps += common.depsList[key]
      deps += ","

  # convert strings to list
  deps = deps.split(',')

  # remove empty items on list
  deps = [x for x in deps if x]

  # remove possible duplicated entries on the list
  deps = set(deps)

  # figure out which dependencies we actually have to build,
  # besides those listed in the TPL from CMake.

  deps_to_build = ""

  for item in deps:

    for pkg in common.packages:

      if ( item.find(pkg.lower()) >= 0 and ( not eval(("common.TPL_HAVE_%s")%pkg) ) ):
        deps_to_build += pkg.lower() + ";"

  # convert strings to list
  deps_to_build = deps_to_build.split(';')

  # remove empty items on list
  deps_to_build = [x for x in deps_to_build if x]

  # The proper TPL build order needs to be respected
  # to resolve possible cyclic dependencies.

  # remove duplicates with set
  allTPL = list( set( TPL + deps_to_build ) )

  # list of ordered dependencies for all packages. If we were to build
  # everythin, this should be the order in which they would be built, as
  # listed in common.packages
  ordered_all_TPL = ";".join([x.lower() for x in common.packages])
  ordered_all_TPL = ordered_all_TPL.split(';')

  index_list = []

  for item in allTPL:

    for i in range(0, len(ordered_all_TPL) ):

      if ( item == ordered_all_TPL[i] ): index_list.append(i)

  # sort the TPL list following the dependency order.
  TPL = [x for (y,x) in sorted(zip(index_list,allTPL))]

  # Assign the ordered, complete dependency tree to the global
  # TPL variable.

  common.TPL = TPL

  return

# ---------------------------------------------------------
