import common
import os, glob
from subprocess import call

# ---------------------------------------------------------

def build():

  # extract source file
  pkgfile = common.TPL_DIR + "/sources/numerics/oce-OCE-0.14.tpxz"
  common.extract_package("OCE", pkgfile)

  # configure, make and install
  patch()

  # change to build dirctory
  dirname = common.buildDir + "/" + "*oce*"
  directory = glob.glob( dirname )
  os.chdir( directory[0] )

  configure()

  if not common.assert_is_ok: return

  common.make_package("OCE")

  if not common.assert_is_ok: return

  common.install_package("OCE")

  if not common.assert_is_ok: return

  define_common_vars()

  return

# ---------------------------------------------------------

def patch():

  print("OCE: patching")

  oce_src_dir = common.baseDir + "/packages/numerics/oce"

  os.chdir( common.buildDir )

  command = "patch -p0 -i %s/oce.patch" %(oce_src_dir)

  call( command, shell=True )

  return

# ---------------------------------------------------------

def configure():

  print("OCE: configure")

  directory = glob.glob( common.buildDir + "/" + "*oce*" )

  oce_src_dir = directory[0]
  common.oce_install_dir = common.installDir + "/oce"

  os.chdir( oce_src_dir )
  os.mkdir( "build" )
  os.chdir( "build" )

  logfile = open( common.buildDir + "/" + "OCE_configure.log", 'w')

  command = "cmake \
  -D OCE_INSTALL_PREFIX:PATH=%s \
  %s \
  -D OCE_DATAEXCHANGE:BOOL=ON \
  -D OCE_DRAW:BOOL=OFF \
  -D OCE_MODEL:BOOL=ON \
  -D OCE_MULTITHREAD_LIBRARY:STRING=NONE \
  -D OCE_OCAF:BOOL=ON \
  -D OCE_VISUALISATION:BOOL=ON \
  -D OCE_WITH_FREEIMAGE:BOOL=OFF \
  -D OCE_WITH_GL2PS:BOOL=OFF \
  %s" \
  %(common.oce_install_dir, common.build_type_cmake,\
    oce_src_dir)

  if ( common.build_libs_as == "shared" ):
    command += " -DOCE_BUILD_SHARED_LIB:BOOL=ON"
  if ( common.build_libs_as == "static" ):
    command += " -DOCE_BUILD_SHARED_LIB:BOOL=OFF"

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): common.assert_is_ok = False

  return

# ---------------------------------------------------------

def define_common_vars():

  common.OCE_INCLUDE_DIR = common.oce_install_dir + "/include"
  common.OCE_LIBRARY = common.oce_install_dir + "/lib/libPTKernel.so"

  return