import common
import os, glob
from subprocess import call

# ---------------------------------------------------------

def build():

  # extract source file
  pkgfile = common.TPL_DIR + "/sources/numerics/scotch_6.0.0.tpxz"
  common.extract_package("scotch", pkgfile)

  # change to build dirctory
  dirname = common.buildDir + "/" + "*scotch*"
  directory = glob.glob( dirname )
  os.chdir( directory[0] )

  # configure, make and install
  configure()

  if not common.assert_is_ok: return

  make()

  if not common.assert_is_ok: return

  install()

  if not common.assert_is_ok: return

  define_common_vars()

  return

# ---------------------------------------------------------

def configure():

  print("scotch: configure")

  directory = glob.glob( common.buildDir + "/" + "*scotch*" )

  scotch_src_dir = directory[0]
  common.scotch_install_dir = common.installDir + "/scotch"

  logfile = open( common.buildDir + "/" + "scotch_configure.log", 'w')

  os.chdir( "src" )

  command = "sed -i \"s,/usr/local,%s,g\" Makefile" %(common.scotch_install_dir)
  cmdstatus = call( command, shell=True )


  # setup library build option
  if ( common.build_libs_as == "shared" ):
    command = "cp -p Make.inc/Makefile.inc.x86-64_pc_linux2.shlib Makefile.inc"
    cmdstatus = call( command, shell=True )
  if ( common.build_libs_as == "static" ):
    command = "cp -p Make.inc/Makefile.inc.x86-64_pc_linux2 Makefile.inc"
    cmdstatus = call( command, shell=True )

  # setup debug flags. Note that the default inc files
  # already use optimized flags.
  if ( common.build_type == "debug" ):
    command = "sed -i \"s,= -O3,= -g -O0,g\" Makefile.inc"
    cmdstatus = call( command, shell=True )


  command = "sed -i \"s,= mpicc,= %s,g\" Makefile.inc" %(common.MPI_C_COMPILER)
  cmdstatus = call( command, shell=True )

  command = "sed -i \"s,-lz -lm -lrt,-lz -lm -lrt -pthread,g\" Makefile.inc"
  cmdstatus = call( command, shell=True )

  command = "sed -i \"/CCD/ c CCD\t\t= %s\" Makefile.inc" %(common.MPI_C_COMPILER)
  cmdstatus = call( command, shell=True )

  logfile.close()

  if ( cmdstatus != 0 ): common.assert_is_ok = False

  return

# ---------------------------------------------------------

def make():

  print("scotch: make")

  logfile = open( common.buildDir + "/" + "scotch_make.log", 'w')

  # Using multiple cores to compile may lead to linker error:
  # truncated files. For now, compile it in serial mode.
  command = "make scotch ptscotch --jobs=1 VERBOSE=1"
  #command = "make scotch ptscotch %s" %( common.MAKEFLAGS )

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): common.assert_is_ok = False

  return

# ---------------------------------------------------------

def install():

  print("scotch: install")

  logfile = open( common.buildDir + "/" + "scotch_install.log", 'w')

  if os.path.exists( common.scotch_install_dir ):
    shutil.rmtree( common.scotch_install_dir )

  os.makedirs( common.scotch_install_dir )

  os.mkdir( common.scotch_install_dir + "/bin" )
  os.mkdir( common.scotch_install_dir + "/include" )
  os.mkdir( common.scotch_install_dir + "/lib" )
  os.mkdir( common.scotch_install_dir + "/share" )
  os.mkdir( common.scotch_install_dir + "/share/man" )

  command = "make install"

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): common.assert_is_ok = False

  return

# ---------------------------------------------------------

def define_common_vars():

  common.SCOTCH_INCLUDE_DIR = common.scotch_install_dir + "/include"
  common.SCOTCH_LIBRARY = common.scotch_install_dir + "/lib/libscotch.so"

  return

