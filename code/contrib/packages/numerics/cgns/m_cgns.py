import common
import os, glob
from subprocess import call

# ---------------------------------------------------------

def build():

  # extract source file
  pkgfile = common.TPL_DIR + "/sources/numerics/cgnslib_3.2.1.tpxz"
  common.extract_package("cgns", pkgfile)

  # change to build dirctory
  dirname = common.buildDir + "/" + "*cgns*"
  directory = glob.glob( dirname )
  os.chdir( directory[0] )

  # configure, make and install
  configure()

  if not common.assert_is_ok: return

  common.make_package("cgns")

  if not common.assert_is_ok: return

  common.install_package("cgns")

  if not common.assert_is_ok: return

  define_common_vars()

  return

# ---------------------------------------------------------

def configure():

  print("cgns: configure")

  directory = glob.glob( common.buildDir + "/" + "*cgns*" )

  cgns_src_dir = directory[0]
  common.cgns_install_dir = common.installDir + "/cgns"

  logfile = open( common.buildDir + "/" + "cgns_configure.log", 'w')

  #cgns_src_dir = directory[0] + "/src"
  #os.chdir( cgns_src_dir )

  ## Proceed with proper configuration
  #command = "./configure \
  #--prefix=%s \
  #--enable-shared \
  #--without-fortran \
  #--disable-cgnstools \
  #--with-mpi \
  #--with-hdf5=%s \
  #--with-zlib \
  #--enable-parallel" \
  #%(common.cgns_install_dir,\
    #common.hdf5_install_dir)

  command = "cmake \
  -D CMAKE_INSTALL_PREFIX:PATH=%s \
  %s \
  -D CMAKE_SKIP_RPATH:BOOL=ON \
  -D CGNS_BUILD_CGNSTOOLS:BOOL=OFF \
  -D CGNS_ENABLE_64BIT:BOOL=ON \
  -D CGNS_ENABLE_FORTRAN:BOOL=OFF \
  -D CGNS_ENABLE_PARALLEL:BOOL=ON \
  -D CGNS_ENABLE_HDF5:BOOL=ON \
  -D HDF5_INCLUDE_PATH:PATH=%s \
  -D HDF5_LIBRARY:FILEPATH=%s \
  -D HDF5_NEED_MPI:BOOL=ON \
  -D HDF5_NEED_ZLIB:BOOL=ON \
  -D ZLIB_LIBRARY:FILEPATH=%s \
  -D CGNS_ENABLE_SCOPING:BOOL=OFF \
  -D CGNS_ENABLE_TESTS:BOOL=OFF \
  -D MPIEXEC:FILEPATH=%s \
  -D MPI_C_COMPILER:FILEPATH=%s \
  -D MPI_C_INCLUDE_PATH:PATH=\"%s\" \
  %s" \
  %(common.cgns_install_dir,\
    common.build_type_cmake,\
    common.HDF5_INCLUDE_DIRS,common.HDF5_LIBRARIES,\
    common.ZLIB_LIBRARIES,\
    common.MPIEXEC,\
    common.MPI_C_COMPILER, \
    common.MPI_C_INCLUDE_PATH, \
    cgns_src_dir)

  if ( common.build_libs_as == "shared" ):
    command += " -DCGNS_BUILD_SHARED:BOOL=ON"
  if ( common.build_libs_as == "static" ):
    command += " -DCGNS_BUILD_SHARED:BOOL=OFF"

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): common.assert_is_ok = False

  return

# ---------------------------------------------------------

def define_common_vars():

  # nothing to do for now.

  return

