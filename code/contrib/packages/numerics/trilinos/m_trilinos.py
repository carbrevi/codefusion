import common
import os, glob
from subprocess import call

# ---------------------------------------------------------

def build():

  # extract source file
  pkgfile = common.TPL_DIR + "/sources/numerics/trilinos-11.4.2-Source.tpxz"
  common.extract_package("trilinos", pkgfile)

  # change to build dirctory
  dirname = common.buildDir + "/" + "*trilinos*"
  directory = glob.glob( dirname )
  os.chdir( directory[0] )

  # configure, make and install
  configure()

  if not common.assert_is_ok: return

  common.make_package("trilinos")

  if not common.assert_is_ok: return

  common.install_package("trilinos")

  if not common.assert_is_ok: return

  define_common_vars()

  return

# ---------------------------------------------------------

def configure():

  print("trilinos: configure")

  directory = glob.glob( common.buildDir + "/" + "*trilinos*" )

  trilinos_src_dir = directory[0]
  common.trilinos_install_dir = common.installDir + "/trilinos"

  # trilinos supports only out-of-source-tree builds.
  os.chdir( trilinos_src_dir )
  os.mkdir( "build" )
  os.chdir( "build" )

  logfile = open( common.buildDir + "/" + "trilinos_configure.log", 'w')

  # Define compiler warning supressions for g++
  # If something goes wrong with the compilation,
  # remove these.
  CXX_Wno_FLAGS = ""
  if ( common.compiler == "gcc" ):
    # Most of these warnings come from Boost headers.
    # The suppression should improve the logfile and
    # possible troubleshoot for the Trilinos TPL.
    CXX_Wno_FLAGS = "-Wno-long-long -Wno-unused-local-typedefs -Wno-unused-variable -Wno-return-type"

  command = "cmake \
  -D CMAKE_INSTALL_PREFIX:PATH=%s \
  %s \
  %s \
  -D CMAKE_CXX_FLAGS:STRING=\"%s\" \
  -D Trilinos_ASSERT_MISSING_PACKAGES=OFF \
  \
  -D Trilinos_ENABLE_EXPLICIT_INSTANTIATION:BOOL=OFF \
  -D Teuchos_ENABLE_LONG_LONG_INT:BOOL=ON \
  \
  -D Trilinos_ENABLE_ALL_PACKAGES:BOOL=OFF \
  \
  -D Trilinos_ENABLE_Teuchos:BOOL=ON \
  -D Trilinos_ENABLE_Tpetra:BOOL=ON \
  -D Trilinos_ENABLE_Kokkos:BOOL=ON \
  -D Trilinos_ENABLE_Thyra:BOOL=ON \
  -D Trilinos_ENABLE_Sacado:BOOL=ON \
  -D Trilinos_ENABLE_Belos:BOOL=ON \
  -D Trilinos_ENABLE_Ifpack:BOOL=ON \
  -D Trilinos_ENABLE_ML:BOOL=ON \
  -D Trilinos_ENABLE_Zoltan2:BOOL=ON \
  \
  -D Trilinos_ENABLE_Fortran:BOOL=OFF \
  -D Trilinos_ENABLE_TESTS:BOOL=OFF \
  -D Trilinos_ENABLE_EXAMPLES:BOOL=OFF \
  \
  -D Trilinos_ENABLE_OpenMP:BOOL=OFF \
  -D TPL_ENABLE_MPI:BOOL=ON \
  \
  -D MPI_CXX_COMPILER:FILEPATH=%s \
  -D MPI_C_COMPILER:FILEPATH=%s \
  -D MPI_EXEC:FILEPATH=%s \
  \
  -D TPL_ENABLE_Zlib:BOOL=ON \
  -D TPL_Zlib_INCLUDE_DIRS:PATH=%s \
  -D TPL_Zlib_LIBRARIES:FILEPATH=\"%s\" \
  \
  -D TPL_ENABLE_Boost:BOOL=ON \
  -D Boost_INCLUDE_DIRS:PATH=%s \
  -D Boost_LIBRARY_DIRS:PATH=%s \
  \
  -D TPL_ENABLE_BLAS:BOOL=ON \
  -D TPL_BLAS_LIBRARIES=\"%s\" \
  -D TPL_ENABLE_LAPACK:BOOL=ON \
  -D TPL_LAPACK_LIBRARIES=\"%s\" \
  \
  -D Zoltan2_ENABLE_Scotch:BOOL=ON \
  -D TPL_ENABLE_Scotch:BOOL=ON \
  -D Scotch_INCLUDE_DIRS:PATH=%s/include \
  -D Scotch_LIBRARY_DIRS:PATH=%s/lib \
  -D Scotch_LIBRARY_NAMES:STRING=\"ptscotch;scotch;ptscotcherr\" \
  %s" \
  %( common.trilinos_install_dir, common.build_type_cmake,\
     common.lib_flag_cmake,\
     CXX_Wno_FLAGS,\
     common.MPI_CXX_COMPILER, \
     common.MPI_C_COMPILER, \
     common.MPIEXEC, \
     common.ZLIB_INCLUDE_DIRS,\
     common.ZLIB_LIBRARIES,\
     common.Boost_INCLUDE_DIRS,\
     common.Boost_ROOT,\
     common.LAPACK_LIBRARIES,\
     common.LAPACK_LIBRARIES,\
     common.scotch_install_dir, common.scotch_install_dir,\
     trilinos_src_dir )

  # if we are using Intel MPI, we need to supress a DEFINE in one of its MPI include.
  if ( common.MPI_CXX_COMPILER.lower().find("intel") >= 0 ):
    command += " -DCMAKE_CXX_FLAGS:STRING=\"-DMPICH_IGNORE_CXX_SEEK\""

  logfile.write( "\n\n command: %s\n" %(command.replace("  ","")) )

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): common.assert_is_ok = False

  return

# ---------------------------------------------------------

def define_common_vars():

  # nothing to do for now.

  return

