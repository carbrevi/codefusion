import common
import os, glob
from subprocess import call

# ---------------------------------------------------------

def build():

  # extract source file
  pkgfile = common.TPL_DIR + "/sources/numerics/tecio2013.tpxz"
  common.extract_package("tecio", pkgfile)

  # configure, make and install
  patch()

  # change to build dirctory
  dirname = common.buildDir + "/" + "*tecio*"
  directory = glob.glob( dirname )
  os.chdir( directory[0] )

  configure()

  if not common.assert_is_ok: return

  common.make_package("tecio")

  if not common.assert_is_ok: return

  common.install_package("tecio")

  if not common.assert_is_ok: return

  define_common_vars()

  return

# ---------------------------------------------------------

def patch():

  print("tecio: patching")

  tecio_src_dir = common.baseDir + "/packages/numerics/tecio"

  os.chdir( common.buildDir )

  command = "patch -p0 -i %s/tecio.patch" %(tecio_src_dir)

  call( command, shell=True )

  return

# ---------------------------------------------------------

def configure():

  print("tecio: configure")

  directory = glob.glob( common.buildDir + "/" + "*tecio*" )

  tecio_src_dir = directory[0] + "/tecsrc"
  common.tecio_install_dir = common.installDir + "/tecio"

  logfile = open( common.buildDir + "/" + "tecio_configure.log", 'w')

  cxxflags = "\"-DLINUX -DLINUX64 -DUSEENUM -DTHREED\""

  command = "cmake \
  -D CMAKE_INSTALL_PREFIX=%s \
  -D CMAKE_CXX_FLAGS:STRING=%s \
  %s \
  %s \
  %s" \
  %( common.tecio_install_dir, cxxflags,\
     common.lib_flag_cmake, common.build_type_cmake,\
     tecio_src_dir )

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): common.assert_is_ok = False

  return

# ---------------------------------------------------------

def define_common_vars():

  # nothing to do for now.

  return

