import common
import os, glob
from subprocess import call
import shutil

# ---------------------------------------------------------

def build():

  # extract source file
  pkgfile = common.TPL_DIR + "/sources/system/zlib-1.2.8.tpxz"
  common.extract_package("zlib", pkgfile)

  # change to build dirctory
  dirname = common.buildDir + "/" + "*zlib*"
  directory = glob.glob( dirname )
  os.chdir( directory[0] )

  # configure, make and install
  configure()

  if not common.assert_is_ok: return

  common.make_package("zlib")

  if not common.assert_is_ok: return

  common.install_package("zlib")

  if not common.assert_is_ok: return

  define_common_vars()

  return

# ---------------------------------------------------------

def configure():

  print("zlib: configure")

  directory = glob.glob( common.buildDir + "/" + "*zlib*" )

  common.zlib_install_dir = common.installDir + "/zlib"

  logfile = open( common.buildDir + "/" + "zlib_configure.log", 'w')

  CFLAGS = "-O2"

  if ( common.build_libs_as == "static" ):
    CFLAGS += " -fPIC"

  command = "CC=%s \
  CFLAGS=\"%s\" \
  ./configure \
  --prefix=%s" \
  %(common.CC, CFLAGS,\
    common.zlib_install_dir)

  if ( common.build_libs_as == "static" ):
    command += " --static"

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): common.assert_is_ok = False

  return

# ---------------------------------------------------------

def define_common_vars():

  common.zlib_install_dir = common.installDir + "/zlib"
  common.ZLIB_ROOT = common.zlib_install_dir

  common.ZLIB_INCLUDE_DIRS = common.zlib_install_dir + "/include"
  common.ZLIB_LIBRARY_DIRS = common.zlib_install_dir + "/lib"

  common.ZLIB_LIBRARIES = common.zlib_install_dir + "/lib/" + common.define_lib_name("z")

  return
