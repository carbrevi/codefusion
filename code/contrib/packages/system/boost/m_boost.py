import common
import os, glob
from subprocess import call

# ---------------------------------------------------------

def build():

  # extract source file
  pkgfile = common.TPL_DIR + "/sources/system/boost_1_55_0.tpxz"
  common.extract_package("boost", pkgfile)

  # configure, make and install
  patch()

  # change to build dirctory
  dirname = common.buildDir + "/" + "*boost*"
  directory = glob.glob( dirname )
  os.chdir( directory[0] )

  configure()

  if not common.assert_is_ok: return

  make()

  if not common.assert_is_ok: return

  define_common_vars()

  return

# ---------------------------------------------------------

def patch():

  print("boost: patching")

  boost_src_dir = common.baseDir + "/packages/system/boost"

  dirname = common.buildDir + "/" + "*boost*"
  boost_build_dir = glob.glob( dirname )

  os.chdir( common.buildDir )

  command = "patch -p0 -i %s/001-log_fix_dump_avx2.patch" %(boost_src_dir)
  call( command, shell=True )

  python_install_dir = common.installDir + "/python"

  boost_conf = str(boost_build_dir[0]) + "/tools/build/v2/user-config.jam"

  with open( boost_conf, 'a') as file:
    # Shut up strict aliasing warnings
    if ( common.compiler == "gcc" ):
      file.write( "using gcc : : : <compileflags>-fno-strict-aliasing ;\n" )
    # Add an extra python version. This does not replace anything and python 2.x need to be the default.
    file.write( ( "using python : 3.3 : %s : %s/include/python3.3m : %s/lib ;\n" ) \
    %(python_install_dir,python_install_dir,python_install_dir ) )
    # Support for OpenMPI
    file.write( "using mpi ;\n" )

  return

# ---------------------------------------------------------

def configure():

  print("boost: configure")

  directory = glob.glob( common.buildDir + "/" + "*boost*" )

  common.boost_install_dir = common.installDir + "/boost"

  logfile = open( common.buildDir + "/" + "boost_configure.log", 'w')

  if ( common.compiler == "gcc" ):
    toolset = "gcc"
  if ( common.compiler == "intel" ):
    toolset = "intel-linux"

  command = "./bootstrap.sh \
  --with-toolset=%s" \
  %( toolset )

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): common.assert_is_ok = False

  return

# ---------------------------------------------------------

def make():

  print("boost: make and install")

  logfile = open( common.buildDir + "/" + "boost_make.log", 'w')

  if ( common.compiler == "gcc" ):
    toolset = "gcc"
  if ( common.compiler == "intel" ):
    toolset = "intel-linux"

  # default "minimal" install: "release link=shared,static
  # runtime-link=shared,static threading=single,multi"
  # --layout=tagged will add the "-mt" suffix for multithreaded libraries
  # and installs includes in $common.installDir/include/boost.
  # --layout=system no longer adds the -mt suffix for multi-threaded libs.
  command = "./b2 \
  toolset=%s \
  python=3.3 \
  --layout=system \
  --prefix=%s \
  --without-mpi \
  --without-python \
  --without-coroutine \
  --without-thread \
  -j %s\
  -sZLIB_SOURCE=%s \
  install" \
  %( toolset, common.boost_install_dir,\
    str(common.NPROCS), common.ZLIB_ROOT )

  if ( common.build_libs_as == "shared" ):
    command += " runtime-link=shared link=shared"
  if ( common.build_libs_as == "static" ):
    command += " link=static"

  if ( common.build_type == "debug" ):
    command += " variant=debug debug-symbols=on"
  if ( common.build_type == "release" ):
    command += " variant=release debug-symbols=off threading=multi \
    cflags=\"${CPPFLAGS} ${CFLAGS} -O3\""

  call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  return

# ---------------------------------------------------------

def define_common_vars():

  common.Boost_ROOT = common.boost_install_dir
  common.Boost_INCLUDE_DIRS = common.boost_install_dir + "/include"

  return

