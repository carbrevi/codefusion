import common
import os, glob
from subprocess import call
import shutil

# ---------------------------------------------------------

def build():

  # Check https://github.com/vasi/pixz
  # for the official repositoty.

  # extract source file
  pkgfile = common.TPL_DIR + "/sources/system/pixz_30dec13.tar.gz"
  common.extract_package("pixz", pkgfile)

  # change to build dirctory
  dirname = common.buildDir + "/" + "*pixz*"
  directory = glob.glob( dirname )
  os.chdir( directory[0] )

  # configure, make and install
  configure()

  if not common.assert_is_ok: return

  common.make_package("pixz")

  if not common.assert_is_ok: return

  install()

  if not common.assert_is_ok: return

  define_common_vars()

  return

# ---------------------------------------------------------

def configure():

  print("pixz: configure")

  directory = glob.glob( common.buildDir + "/" + "*pixz*" )

  common.pixz_install_dir = common.installDir + "/pixz"

  logfile = open( common.buildDir + "/" + "pixz_configure.log", 'w')

  # set compiler
  if ( common.compiler == "intel" ):
    command = "sed -i \"s,CC = gcc,CC = icc,g\" Makefile"
    cmdstatus = call( command, shell=True )
    if ( cmdstatus != 0 ): common.assert_is_ok = False

  if ( common.build_type == "release" ):
    command = "sed -i \"s,OPT = -g -O0,OPT = -O2,g\" Makefile"
    cmdstatus = call( command, shell=True )
    if ( cmdstatus != 0 ): common.assert_is_ok = False

  logfile.close()

  return

# ---------------------------------------------------------

def install():

  print("pixz: install")

  if os.path.exists( common.pixz_install_dir ):
    shutil.rmtree( common.pixz_install_dir )

  os.makedirs( common.pixz_install_dir )

  pixz_install_dir_bin = common.pixz_install_dir + "/bin"
  os.mkdir( pixz_install_dir_bin )

  # copy executable to destination, preserving file attributes
  shutil.copy2("pixz", pixz_install_dir_bin)

  return

# ---------------------------------------------------------

def define_common_vars():

  common.PIXZ_EXECUTABLE = common.installDir + "/pixz/bin/pixz"

  return
