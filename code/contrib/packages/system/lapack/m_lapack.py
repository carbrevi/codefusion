import common
import os, glob
from subprocess import call
# ---------------------------------------------------------
# ATTENTION #
# BLAS comes bundled with the present LAPACK distribution
# ---------------------------------------------------------

def build():

  # extract source file
  pkgfile = common.TPL_DIR + "/sources/system/lapack-3.5.0.tpxz"
  common.extract_package("blas/lapack", pkgfile)

  # change to build dirctory
  dirname = common.buildDir + "/" + "*lapack*"
  directory = glob.glob( dirname )
  os.chdir( directory[0] )

  # configure, make and install
  configure()

  if not common.assert_is_ok: return

  common.make_package("lapack")

  if not common.assert_is_ok: return

  common.install_package("lapack")

  if not common.assert_is_ok: return

  define_common_vars()

  return

# ---------------------------------------------------------

def configure():

  print("lapack: configure")
  print("NOTE: BLAS comes bundled with lapack.")

  directory = glob.glob( common.buildDir + "/" + "*lapack*" )

  lapack_src_dir = directory[0]
  common.lapack_install_dir = common.installDir + "/lapack"

  logfile = open( common.buildDir + "/" + "lapack_configure.log", 'w')

  cxxflags = "\"-DLINUX -DLINUX64 -DUSEENUM -DTHREED\""

  command = "cmake \
  -D CMAKE_INSTALL_PREFIX:PATH=%s \
  %s \
  %s \
  -D CMAKE_SKIP_RPATH:BOOL=ON \
  -D BUILD_TESTING:BOOL=OFF \
  %s" \
  %( common.lapack_install_dir,\
     common.build_type_cmake,\
     common.lib_flag_cmake,\
     lapack_src_dir )

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): common.assert_is_ok = False

  return

# ---------------------------------------------------------

def define_common_vars():

  common.LAPACK_LIBRARIES = common.lapack_install_dir + "/lib/" + common.define_lib_name("lapack")
  common.LAPACK_LIBRARIES += ";" + common.lapack_install_dir + "/lib/" + common.define_lib_name("blas")

  return

