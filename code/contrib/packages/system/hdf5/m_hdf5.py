import common
import os, glob
from subprocess import call

# ---------------------------------------------------------

def build():

  # extract source file
  pkgfile = common.TPL_DIR + "/sources/system/hdf5-1.8.12.tpxz"
  common.extract_package("hdf5", pkgfile)

  # change to build dirctory
  dirname = common.buildDir + "/" + "*hdf5*"
  directory = glob.glob( dirname )
  os.chdir( directory[0] )

  # configure, make and install
  configure()

  if not common.assert_is_ok: return

  common.make_package("hdf5")

  if not common.assert_is_ok: return

  common.install_package("hdf5")

  if not common.assert_is_ok: return

  define_common_vars()

  return

# ---------------------------------------------------------

def configure():

  print("hdf5: configure")

  directory = glob.glob( common.buildDir + "/" + "*hdf5*" )

  hdf5_src_dir = directory[0] + "/source"
  common.hdf5_install_dir = common.installDir + "/hdf5"

  logfile = open( common.buildDir + "/" + "hdf5_configure.log", 'w')

  command = "./configure \
  CC=%s \
  CXX=%s \
  RUNPARALLEL=%s \
  OMPI_MCA_disable_memory_allocator=1 \
  --prefix=%s \
  --enable-largefile \
  --enable-unsupported \
  %s \
  --with-zlib=%s,%s \
  --with-pthread=%s,%s \
  --with-default-api-version=v18 \
  --enable-parallel=yes \
  --enable-cxx \
  --disable-fortran \
  --disable-sharedlib-rpath" \
  %(common.MPI_C_COMPILER,\
    common.MPI_CXX_COMPILER,\
    common.MPIEXEC,\
    common.hdf5_install_dir,\
    common.lib_flag_autotools,\
    common.ZLIB_INCLUDE_DIRS,common.ZLIB_LIBRARIES,\
    common.PTHREADS_INCLUDE_DIR,common.PTHREADS_LIBRARY)

  if ( common.build_type == "debug" ):
    command += " --enable-debug=all"
  if ( common.build_type == "release" ):
    command += " --enable-production=yes"

  # if we are using Intel MPI, we need to supress a DEFINE in one of its MPI include.
  if ( common.MPI_CXX_COMPILER.lower().find("intel") >= 0 ):
    command += " CXXFLAGS=\"-DMPICH_IGNORE_CXX_SEEK\""

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): common.assert_is_ok = False

  return

# ---------------------------------------------------------

def define_common_vars():

  common.HDF5_INCLUDE_DIRS = common.hdf5_install_dir + "/include"
  common.HDF5_LIBRARY_DIRS = common.hdf5_install_dir + "/lib"

  common.HDF5_LIBRARIES = common.hdf5_install_dir + "/lib/" + common.define_lib_name("hdf5")

  return
