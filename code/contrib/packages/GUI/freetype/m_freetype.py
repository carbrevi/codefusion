import common
import os, glob
from subprocess import call

# ---------------------------------------------------------

def build():

  # extract source file
  pkgfile = common.TPL_DIR + "/sources/GUI/freetype-2.5.0.1.tpxz"
  common.extract_package("freetype", pkgfile)

  # change to build dirctory
  dirname = common.buildDir + "/" + "*freetype*"
  directory = glob.glob( dirname )
  os.chdir( directory[0] )

  # configure, make and install
  configure()

  if not common.assert_is_ok: return

  common.make_package("freetype")

  if not common.assert_is_ok: return

  common.install_package("freetype")

  if not common.assert_is_ok: return

  define_common_vars()

  return

# ---------------------------------------------------------

def configure():

  print("freetype: configure")

  directory = glob.glob( common.buildDir + "/" + "*freetype*" )

  common.freetype_install_dir = common.installDir + "/freetype"

  logfile = open( common.buildDir + "/" + "freetype_configure.log", 'w')

  CFLAGS = "-O2"

  if ( common.build_libs_as == "static" ):
    CFLAGS += " -fPIC"

  command = "LIBPNG_LDFLAGS=\"-L%s -lpng\" \
  CC=%s \
  CXX=%s \
  CFLAGS=\"%s\" \
  ./configure \
  --prefix=%s \
  %s \
  --with-zlib=%s \
  --without-bzip2" \
  %(common.PNG_LIBRARY_DIRS,\
    common.CC,\
    common.CXX,\
    CFLAGS,\
    common.freetype_install_dir,\
    common.lib_flag_autotools,\
    common.ZLIB_LIBRARIES)

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): common.assert_is_ok = False

  return

# ---------------------------------------------------------

def define_common_vars():

  common.FREETYPE_INCLUDE_DIRS = common.freetype_install_dir + "/include"
  common.FREETYPE_LIBRARY_DIRS = common.freetype_install_dir + "/lib"
  common.FREETYPE_LIBRARIES = common.freetype_install_dir + "/lib/libfreetype.so"

  return

# ---------------------------------------------------------
