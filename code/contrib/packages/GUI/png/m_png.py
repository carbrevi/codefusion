import common
import os, glob
from subprocess import call

# ---------------------------------------------------------

def build():

  # extract source file
  pkgfile = common.TPL_DIR + "/sources/GUI/libpng-1.6.6.tpxz"
  common.extract_package("png", pkgfile)

  # change to build dirctory
  dirname = common.buildDir + "/" + "*libpng*"
  directory = glob.glob( dirname )
  os.chdir( directory[0] )

  # configure, make and install
  configure()

  if not common.assert_is_ok: return

  common.make_package("png")

  if not common.assert_is_ok: return

  common.install_package("png")

  if not common.assert_is_ok: return

  define_common_vars()

  return

# ---------------------------------------------------------

def configure():

  print("png: configure")

  directory = glob.glob( common.buildDir + "/" + "*png*" )

  png_src_dir = directory[0] + "/source"
  common.png_install_dir = common.installDir + "/png"

  logfile = open( common.buildDir + "/" + "png_configure.log", 'w')

  CFLAGS = "-O2"

  if ( common.build_libs_as == "static" ):
    CFLAGS += " -fPIC"

  command = "./configure \
  CC=%s \
  CXX=%s \
  CFLAGS=\"%s\" \
  --prefix=%s \
  %s \
  --with-zlib-prefix=%s" \
  %(common.CC,\
    common.CXX,\
    CFLAGS,\
    common.png_install_dir,\
    common.lib_flag_autotools,\
    common.zlib_install_dir)

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): common.assert_is_ok = False

  return

# ---------------------------------------------------------

def define_common_vars():

  common.PNG_INCLUDE_DIRS = common.png_install_dir + "/include"
  common.PNG_LIBRARY_DIRS = common.png_install_dir + "/lib"
  common.PNG_LIBRARIES = common.png_install_dir + "/lib/libpng.so"

  return

# ---------------------------------------------------------
