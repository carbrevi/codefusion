import common
import os, glob
from subprocess import call

# ---------------------------------------------------------

def build():

  # extract source file
  pkgfile = common.TPL_DIR + "/sources/GUI/OpenSceneGraph-3.2.0.tpxz"
  common.extract_package("OSG", pkgfile)

  # patch sources
  patch()

  # change to build dirctory
  dirname = common.buildDir + "/" + "*OpenSceneGraph*"
  directory = glob.glob( dirname )
  os.chdir( directory[0] )

  # configure, make and install
  configure()

  if not common.assert_is_ok: return

  common.make_package("OSG")

  if not common.assert_is_ok: return

  common.install_package("OSG")

  if not common.assert_is_ok: return

  define_common_vars()

  return

# ---------------------------------------------------------

def patch():

  print("OSG: patching")

  osg_src_dir = common.baseDir + "/packages/GUI/osg"

  os.chdir( common.buildDir )

  command = "patch -p0 -i %s/osg.patch" %(osg_src_dir)

  call( command, shell=True )

  return

# ---------------------------------------------------------

def configure():

  print("OSG: configure")

  directory = glob.glob( common.buildDir + "/" + "*OpenSceneGraph*" )

  osg_src_dir = directory[0]
  common.osg_install_dir = common.installDir + "/osg"

  os.chdir( osg_src_dir )
  os.mkdir( "build" )
  os.chdir( "build" )

  logfile = open( common.buildDir + "/" + "OSG_configure.log", 'w')

  command = "cmake \
  -D CMAKE_BUILD_TYPE:STRING=Release \
  -D CMAKE_INSTALL_PREFIX:PATH=%s \
  \
  -D BUILD_DASHBOARD_REPORTS:BOOL=OFF \
  -D BUILD_DOCUMENTATION:BOOL=OFF \
  -D BUILD_OPENTHREADS_WITH_QT:BOOL=OFF \
  -D BUILD_OSG_APPLICATIONS:BOOL=OFF \
  -D BUILD_OSG_EXAMPLES:BOOL=OFF \
  -D BUILD_OSG_PACKAGES:BOOL=OFF \
  -D BUILD_PRESENT3D_WITH_SDL:BOOL=OFF \
  -D OSG_BUILD_PLATFORM_ANDROID:BOOL=OFF \
  -D OSG_CPP_EXCEPTIONS_AVAILABLE:BOOL=ON \
  -D OSG_MAINTAINER:BOOL=OFF \
  -D OSG_NOTIFY_DISABLED:BOOL=OFF \
  \
  -D OSG_USE_FLOAT_BOUNDINGBOX:BOOL=OFF \
  -D OSG_USE_FLOAT_BOUNDINGSPHERE:BOOL=OFF \
  -D OSG_USE_FLOAT_MATRIX:BOOL=OFF \
  -D OSG_USE_FLOAT_PLANE:BOOL=OFF \
  \
  -D OSG_USE_QT:BOOL=ON \
  -D Qt5Core_DIR:PATH=%s/cmake/Qt5Core \
  -D Qt5Gui_DIR:PATH=%s/cmake/Qt5Gui \
  -D Qt5Network_DIR:PATH=%s/cmake/Qt5Network \
  -D Qt5OpenGL_DIR:PATH=%s/cmake/Qt5OpenGL \
  -D Qt5PrintSupport_DIR:PATH=%s/cmake/Qt5PrintSupport \
  -D Qt5Qml_DIR:PATH=%s/cmake/Qt5Qml \
  -D Qt5Quick_DIR:PATH=%s/cmake/Qt5Quick \
  -D Qt5Sensors_DIR:PATH=%s/cmake/Qt5Sensors \
  -D Qt5WebKitWidgets_DIR:PATH=%s/cmake/Qt5WebKitWidgets \
  -D Qt5WebKit_DIR:PATH=%s/cmake/Qt5WebKit \
  -D Qt5Widgets_DIR:PATH=%s/cmake/Qt5Widgets \
  \
  -D Boost_DIR:PATH=%s \
  -D Boost_INCLUDE_DIR:PATH=%s \
  \
  -D OPENGL_INCLUDE_DIR:PATH=%s \
  -D OPENGL_gl_LIBRARY:FILEPATH=%s \
  -D OPENGL_glu_LIBRARY:FILEPATH=%s \
  \
  -D FREETYPE_INCLUDE_DIR_freetype2:PATH=%s/freetype2 \
  -D FREETYPE_INCLUDE_DIR_ft2build:PATH=%s \
  -D FREETYPE_LIBRARY:FILEPATH=%s \
  \
  -D JPEG_INCLUDE_DIR:PATH=%s \
  -D JPEG_LIBRARY:FILEPATH=%s \
  \
  -D PNG_PNG_INCLUDE_DIR:PATH=%s \
  -D PNG_LIBRARY_RELEASE:FILEPATH=%s \
  \
  -D ZLIB_INCLUDE_DIR:PATH=%s \
  -D ZLIB_LIBRARY:FILEPATH=%s \
  %s" \
  %(common.osg_install_dir,\
    common.QT5_LIBRARY_DIRS,common.QT5_LIBRARY_DIRS,\
    common.QT5_LIBRARY_DIRS,common.QT5_LIBRARY_DIRS,\
    common.QT5_LIBRARY_DIRS,common.QT5_LIBRARY_DIRS,\
    common.QT5_LIBRARY_DIRS,common.QT5_LIBRARY_DIRS,\
    common.QT5_LIBRARY_DIRS,common.QT5_LIBRARY_DIRS,\
    common.QT5_LIBRARY_DIRS,\
    common.Boost_ROOT,common.Boost_INCLUDE_DIRS,\
    common.OpenGL_INCLUDE_DIRS,\
    common.OpenGL_gl_LIBRARY,common.OpenGL_glu_LIBRARY,\
    common.FREETYPE_INCLUDE_DIRS,common.FREETYPE_INCLUDE_DIRS,\
    common.FREETYPE_LIBRARIES,\
    common.JPEG_INCLUDE_DIRS,common.JPEG_LIBRARIES,\
    common.PNG_INCLUDE_DIRS,common.PNG_LIBRARIES,\
    common.ZLIB_INCLUDE_DIRS,common.ZLIB_LIBRARIES,\
    osg_src_dir)

  if ( common.build_libs_as == "shared" ):
    command += " -DDYNAMIC_OPENSCENEGRAPH:BOOL=ON"
    command += " -DDYNAMIC_OPENTHREADS:BOOL=ON"
  if ( common.build_libs_as == "static" ):
    command += " -DDYNAMIC_OPENSCENEGRAPH:BOOL=OFF"
    command += " -DDYNAMIC_OPENTHREADS:BOOL=OFF"
    command += " -DCMAKE_CXX_FLAGS:STRING=\"-fPIC\""

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): common.assert_is_ok = False

  return

# ---------------------------------------------------------

def define_common_vars():

  common.OSG_INCLUDE_DIR = common.osg_install_dir + "/include"
  common.OSG_LIBRARY = common.osg_install_dir + "/lib/xxx.so"

  return
