import common
import os, glob
from subprocess import call

# ---------------------------------------------------------

def build():

  # extract source file
  pkgfile = common.TPL_DIR + "/sources/GUI/qt-everywhere-opensource-src-5.2.0.tpxz"
  common.extract_package("qt5", pkgfile)

  # change to build dirctory
  dirname = common.buildDir + "/" + "*qt-everywhere*"
  directory = glob.glob( dirname )
  os.chdir( directory[0] )

  # configure, make and install

  if ( common.build_libs_as == "static" ):
    print("WARNING: LGPL allows only shared libraries.")
    print("Reverting to shared lib build.")

  configure()

  if not common.assert_is_ok: return

  common.make_package("qt5")

  if not common.assert_is_ok: return

  common.install_package("qt5")

  if not common.assert_is_ok: return

  define_common_vars()

  return

# ---------------------------------------------------------

def configure():

  print("qt5: configure")

  directory = glob.glob( common.buildDir + "/" + "*qt-everywhere*" )

  qt5_src_dir = directory[0]
  common.qt5_install_dir = common.installDir + "/qt5"

  os.chdir( qt5_src_dir )

  logfile = open( common.buildDir + "/" + "qt5_configure.log", 'w')

  if ( common.compiler == "gcc" ):
    qtplatform = "linux-g++"
  if ( common.compiler == "intel" ):
    qtplatform = "linux-icc"

  command = "./configure \
  -platform %s \
  -prefix %s \
  -shared \
  -verbose \
  -opensource \
  -confirm-license \
  -gui \
  -opengl desktop \
  -widgets \
  -no-nis \
  -no-cups \
  -no-iconv \
  -no-icu \
  -no-fontconfig \
  -no-dbus \
  -no-gif \
  -no-openssl \
  -nomake examples \
  -nomake tests \
  -nomake tools \
  -no-gtkstyle \
  -no-javascript-jit \
  -no-qml-debug \
  -system-zlib \
  -system-libjpeg \
  -system-libpng \
  -system-freetype \
  -I %s/include \
  -L %s/lib \
  -I %s/include \
  -L %s/lib \
  -I %s/include \
  -L %s/lib \
  -I %s/include \
  -I %s/include/freetype2 \
  -L %s/lib" \
  %(qtplatform,common.qt5_install_dir,\
    common.png_install_dir,common.png_install_dir,\
    common.jpeg_install_dir,common.jpeg_install_dir,\
    common.zlib_install_dir,common.zlib_install_dir,\
    common.freetype_install_dir,common.freetype_install_dir,\
    common.freetype_install_dir)

  if ( common.build_type == "release" ):
    command += " -release -optimized-qmake"
  if ( common.build_type == "debug" ):
    command += " -debug"

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): common.assert_is_ok = False

  return

# ---------------------------------------------------------

def define_common_vars():

  common.QT5_INCLUDE_DIRS = common.qt5_install_dir + "/include"
  common.QT5_LIBRARY_DIRS = common.qt5_install_dir + "/lib"
  common.QT5_LIBRARIES = ""

  return

