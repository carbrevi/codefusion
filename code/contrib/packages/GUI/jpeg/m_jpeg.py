import common
import os, glob
from subprocess import call

# ---------------------------------------------------------

def build():

  # extract source file
  pkgfile = common.TPL_DIR + "/sources/GUI/jpegsrc_v9.tpxz"
  common.extract_package("jpeg", pkgfile)

  # change to build dirctory
  dirname = common.buildDir + "/" + "*jpeg*"
  directory = glob.glob( dirname )
  os.chdir( directory[0] )

  # configure, make and install
  configure()

  if not common.assert_is_ok: return

  common.make_package("jpeg")

  if not common.assert_is_ok: return

  common.install_package("jpeg")

  if not common.assert_is_ok: return

  define_common_vars()

  return

# ---------------------------------------------------------

def configure():

  print("jpeg: configure")

  directory = glob.glob( common.buildDir + "/" + "*jpeg*" )

  jpeg_src_dir = directory[0] + "/source"
  common.jpeg_install_dir = common.installDir + "/jpeg"

  logfile = open( common.buildDir + "/" + "jpeg_configure.log", 'w')

  CFLAGS = "-O2"

  if ( common.build_libs_as == "static" ):
    CFLAGS += " -fPIC"

  command = "./configure \
  CC=%s \
  CXX=%s \
  CFLAGS=\"%s\" \
  --prefix=%s \
  %s" \
  %(common.CC,\
    common.CXX,\
    CFLAGS,\
    common.jpeg_install_dir,\
    common.lib_flag_autotools)

  cmdstatus = call( command, shell=True, stdout=logfile, stderr=logfile )

  logfile.close()

  if ( cmdstatus != 0 ): common.assert_is_ok = False

  return

# ---------------------------------------------------------

def define_common_vars():

  common.JPEG_INCLUDE_DIRS = common.jpeg_install_dir + "/include"
  common.JPEG_LIBRARY_DIRS = common.jpeg_install_dir + "/lib"
  common.JPEG_LIBRARIES = common.jpeg_install_dir + "/lib/libjpeg.so"

  return

# ---------------------------------------------------------
