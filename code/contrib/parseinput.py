import sys, os
import common

# ---------------------------------------------------------

def read():

  # /////////////////////////////////
  # first get intallDir location
  inputFile = "../make.inp"

  if os.path.exists(inputFile):
    inputList = {}
    f = open(inputFile)
    data = f.readlines()
    for line in data:
      # parse input, assign values to variables
      if not line.startswith("#") and not line.startswith("\n"):
        key, value = line.split("=")
        inputList[key.strip()] = value.strip()
    f.close()
  else:
    print("\nError: make.inp not found!\n")
    sys.exit()

  # parse input list
  for item in inputList:
    if ( item == "installDir"     ): common.installDir     = str(inputList[item])
    if ( item == "platform_name"  ): common.platform_name  = str(inputList[item])
    if ( item == "platform_arch"  ): common.platform_arch  = str(inputList[item])
    if ( item == "build_type"     ): common.build_type     = str(inputList[item])
    if ( item == "build_libs_as"  ): common.build_libs_as  = str(inputList[item])
    if ( item == "build_with_mpi" ): common.build_with_mpi = str(inputList[item])
    if ( item == "build_GUI"      ): common.build_GUI      = str(inputList[item])
    if ( item == "build_tests"    ): common.build_tests    = str(inputList[item])
    if ( item == "real_type"      ): common.real_type      = str(inputList[item])
    if ( item == "compiler" ):
      common.compiler = str(inputList[item])
      common.compiler_default = str(inputList[item])

  f.close()

  # /////////////////////////////////
  # read actual build parameters

  inputFile = common.installDir + "/" + common.platform_name
  inputFile = inputFile + "_" + common.platform_arch + "/"
  inputFile = inputFile + common.build_type + "/_build"
  inputFile = inputFile + "/contrib.inp"

  inputList = {}
  f = open(inputFile)
  data = f.readlines()
  for line in data:
    # parse input, assign values to variables
    if not line.startswith("#"):
      key, value = line.split("=")
      inputList[key.strip()] = value.strip()
  f.close()

  # parse input list
  for item in inputList:

    if ( item == "CC" ): common.CC = inputList[item]
    if ( item == "CXX" ): common.CXX = inputList[item]
    if ( item == "FC" ): common.FC = inputList[item]
    if ( item == "MPI_CC" ): common.MPI_CC = inputList[item]
    if ( item == "MPI_CXX" ): common.MPI_CXX = inputList[item]
    if ( item == "MPI_F90" ): common.MPI_F90 = inputList[item]

    if ( item == "TPL_DIR" ): common.TPL_DIR = inputList[item]

    if ( item == "install_prefix" ): common.installDir = inputList[item]

    if ( item == "TPL" ): common.TPL = inputList[item]

    if ( item == "MPI_CXX_COMPILER" ): common.MPI_CXX_COMPILER = inputList[item]
    if ( item == "MPI_C_COMPILER" ): common.MPI_C_COMPILER = inputList[item]
    if ( item == "MPI_Fortran_COMPILER" ): common.MPI_Fortran_COMPILER = inputList[item]
    if ( item == "MPI_C_INCLUDE_PATH" ): common.MPI_C_INCLUDE_PATH = inputList[item]
    if ( item == "MPI_CXX_INCLUDE_PATH" ): common.MPI_CXX_INCLUDE_PATH = inputList[item]
    if ( item == "MPI_Fortran_INCLUDE_PATH" ): common.MPI_Fortran_INCLUDE_PATH = inputList[item]
    if ( item == "MPI_C_LIBRARIES" ): common.MPI_C_LIBRARIES = inputList[item]
    if ( item == "MPI_CXX_LIBRARIES" ): common.MPI_CXX_LIBRARIES = inputList[item]
    if ( item == "MPI_Fortran_LIBRARIES" ): common.MPI_Fortran_LIBRARIES = inputList[item]
    if ( item == "MPIEXEC" ): common.MPIEXEC = inputList[item]

    if ( item == "PTHREADS_INCLUDE_DIR" ): common.PTHREADS_INCLUDE_DIR = inputList[item]
    if ( item == "PTHREADS_LIBRARY" ): common.PTHREADS_LIBRARY = inputList[item]

    if ( item == "OpenGL_INCLUDE_DIRS" ): common.OpenGL_INCLUDE_DIRS = inputList[item]
    if ( item == "OpenGL_LIBRARIES" ): common.OpenGL_LIBRARIES = inputList[item]
    if ( item == "OpenGL_gl_LIBRARY" ): common.OpenGL_gl_LIBRARY = inputList[item]
    if ( item == "OpenGL_glu_LIBRARY" ): common.OpenGL_glu_LIBRARY = inputList[item]

    if ( item == "PIXZ_EXECUTABLE" ): common.PIXZ_EXECUTABLE = inputList[item]


    # we must skip GUI packages if the user does not need
    # to buil them.
    #
    if ( common.build_GUI == "true" ):

      for TPL in common.packages:

        if ( item == ("TPL_HAVE_%s")%TPL ):
          if ( inputList[item] == "ON"  ): setattr(common,('TPL_HAVE_%s')%TPL,True)
          if ( inputList[item] == "OFF" ): setattr(common,('TPL_HAVE_%s')%TPL,False)

        if ( item == ("%s_INCLUDE_DIRS")%TPL ):
          setattr(common,('%s_INCLUDE_DIRS')%TPL,inputList[item])

        if ( item == ("%s_LIBRARIES")%TPL ):
          setattr(common,('%s_LIBRARIES')%TPL,inputList[item])

        if ( item == ("%s_ROOT")%TPL ):
          setattr(common,('%s_ROOT')%TPL,inputList[item])

    else:

      for TPL in common.packages:

        for gui_pkg in common.packages_gui:

          if ( TPL.lower() != gui_pkg.lower() ):

            if ( item == ("TPL_HAVE_%s")%TPL ):
              if ( inputList[item] == "ON"  ): setattr(common,('TPL_HAVE_%s')%TPL,True)
              if ( inputList[item] == "OFF" ): setattr(common,('TPL_HAVE_%s')%TPL,False)

            if ( item == ("%s_INCLUDE_DIRS")%TPL ):
              setattr(common,('%s_INCLUDE_DIRS')%TPL,inputList[item])

            if ( item == ("%s_LIBRARIES")%TPL ):
              setattr(common,('%s_LIBRARIES')%TPL,inputList[item])

            if ( item == ("%s_ROOT")%TPL ):
              setattr(common,('%s_ROOT')%TPL,inputList[item])


  # define some default values for the compiler options.

  common.CC_default = common.CC
  common.CXX_default = common.CXX
  common.FC_default = common.FC

  common.MPI_CC_default = common.MPI_CC
  common.MPI_CXX_default = common.MPI_CXX
  common.MPI_F90_default = common.MPI_F90

  return

# ---------------------------------------------------------
