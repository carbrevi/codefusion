#!/usr/bin/python -B
#
# Invoking python with the B switch turns off bytecode
# generation in the source tree.
#
# This script builds all 3rd-party libraries and installs
# them to the location provided by the user.

from __future__ import print_function

import os, sys, platform, tarfile, glob, shutil, time
from os.path import basename
import multiprocessing
from subprocess import call

# ---------------------------------------------------------
# Parse Input parameters

inputFile = "make.inp"

if os.path.exists(inputFile):
  inputList = {}
  f = open(inputFile)
  data = f.readlines()
  for line in data:
    # parse input, assign values to variables
    if not line.startswith("#") and not line.startswith("\n"):
      key, value = line.split("=")
      inputList[key.strip()] = value.strip()
  f.close()
else:
  print("\nError: make.inp not found!\n")
  sys.exit()

# parse input list
for item in inputList:
  if ( item == "installDir"     ): installDir     = str(inputList[item])
  if ( item == "TPLDir"         ): TPLDir         = str(inputList[item])
  if ( item == "platform_name"  ): platform_name  = str(inputList[item])
  if ( item == "platform_arch"  ): platform_arch  = str(inputList[item])
  if ( item == "build_type"     ): build_type     = str(inputList[item])
  if ( item == "build_libs_as"  ): build_libs_as  = str(inputList[item])
  if ( item == "build_with_mpi" ): build_with_mpi = str(inputList[item])
  if ( item == "build_GUI"      ): build_GUI      = str(inputList[item])
  if ( item == "build_tests"    ): build_tests    = str(inputList[item])
  if ( item == "real_type"      ): real_type      = str(inputList[item])
  if ( item == "compiler"       ): compiler       = str(inputList[item])

if ( compiler == "gcc" ):
  CC  = "gcc"
  CXX = "g++"
  FC  = "gfortran"
  MPI_CC  = "mpicc"
  MPI_CXX = "mpicxx"
  MPI_FC  = "mpif90"
elif ( compiler == "intel" ):
  CC  = "icc"
  CXX = "icpc"
  FC  = "ifort"
  MPI_CC  = "mpicc"
  MPI_CXX = "mpicxx"
  MPI_FC  = "mpif90"

# ---------------------------------------------------------
# Script initialization

startTime = time.time()

# get number of processors for parallel make compilation (shared memory).
NPROCS = multiprocessing.cpu_count()

# get current directory
baseDir = os.getcwd()

# get project source directory
srcDir = baseDir + "/src"

# reset installDir to fit setup options
installDir  = installDir + "/" + platform_name + "_" + platform_arch
installDir += "/" + build_type

# building directory
buildDir = installDir + "/_build/codeFusion"

# contribution directory (TPLs)
contribDir = installDir + "/contrib"

# make flags
MAKEFLAGS = "VERBOSE=1 --jobs=" + str(NPROCS)

# detect OS
OS = platform.system()

# set platform specifics
if OS == "Linux":
  cmake_flags = ""
  def clear(): os.system('clear')
  make_cmd="make"

# get python version
if sys.version_info < (3, 0):
  python_ver = 2
elif sys.version_info >= (3, 0):
  python_ver = 3

clear()

# ---------------------------------------------------------
# Set and get compiler suite info

os.environ['CC'] = CC
os.environ['CXX'] = CXX
os.environ['FC'] = FC
os.environ['MPI_CC'] = CC
os.environ['MPI_CXX'] = CXX
os.environ['MPI_F90'] = FC

# Intel compilers can not compile the GUI packages. Warn user
# if such is the case.
if ( compiler.lower() == "intel" and build_GUI.lower() == "true" ):
  print("---------------------------------------------------------")
  print("WARNING: GUI can only be built with \"gcc\" compiler")
  print("suite at this moment, due to qt5 incompatibilities")
  print("with intel compilers.")
  print("---------------------------------------------------------")
  if python_ver == 2:
    noGUI = raw_input("Disable build_GUI flag? [Y/n]: ")
  elif python_ver == 3:
    noGUI = input("Disable build_GUI flag? [Y/n]: ")

  # set default behavior to disable build_GUI flag.
  if ( noGUI != ""):
    print(noGUI)
    if ( noGUI != "Y" and noGUI != "y" ):
      disable_GUI = False
    else:
      disable_GUI = True
  else:
    disable_GUI = True
    build_GUI = "false"

  if ( not disable_GUI):
    print("Aborting compilation process. Exiting now...")
    sys.exit(1)

# ---------------------------------------------------------
# Inform user about build environment
if OS == "Linux":
  print("*****************************************************")
  print("Developer environment:")
  print("*****************************************************")
  print("C     ", os.environ['CC'])
  print("CXX   ", os.environ['CXX'])
  print("FC    ", os.environ['FC'])
  print("NPROCS", str(NPROCS))
  print("*****************************************************")

# ---------------------------------------------------------
# Remove previous installation, if any

binDir = installDir + "/bin"
libDir = installDir + "/lib"
incDir = installDir + "/include"
shrDir = installDir + "/share"
arcDir = installDir + "/archive"

if os.path.exists( binDir ):
  shutil.rmtree( binDir )
if os.path.exists( libDir ):
  shutil.rmtree( libDir )
if os.path.exists( incDir ):
  shutil.rmtree( incDir )
if os.path.exists( shrDir ):
  shutil.rmtree( shrDir )
if os.path.exists( arcDir ):
  shutil.rmtree( arcDir )

# Remove previous builds, if any

if os.path.exists( buildDir ):
  shutil.rmtree( buildDir )

os.makedirs( buildDir )

# ---------------------------------------------------------
# Configure project

os.chdir( buildDir )

if ( build_libs_as == "shared" ):
  SHARED_LIBS = "TRUE"
else:
  SHARED_LIBS = "FALSE"

print("codeFusion: configure")

conflog = open( buildDir + "/../codeFusion_configure.log", 'w')
comdlog = open( buildDir + "/../codeFusion_command.log", 'w')

#-Wno-dev # supress CMake warnings

command = "cmake \
-DCMAKE_INSTALL_PREFIX=%s \
-DCMAKE_BUILD_TYPE:STRING=%s \
-DBUILD_SHARED_LIBS:BOOL=%s \
-DTPL_DIR=%s \
-DBUILD_WITH_MPI=%s \
-DBUILD_GUI=%s \
-DBUILD_TESTS=%s \
-DREAL_PRECISION=%s \
%s" \
%( installDir, build_type, SHARED_LIBS,\
  TPLDir,\
  build_with_mpi, build_GUI, build_tests,\
  real_type,\
  srcDir )


# once the TPLs are built, we can include them into the cmake command line.

if os.path.exists( contribDir + "/pixz" ):
  command += " -DPIXZ_ROOT:PATH=%s/pixz" %(contribDir)

if os.path.exists( contribDir + "/zlib" ):
  command += " -DZLIB_ROOT:PATH=%s/zlib" %(contribDir)

if os.path.exists( contribDir + "/openmpi" ):
  command += " -DMPI_C_COMPILER:FILEPATH=%s/openmpi/bin/mpicc" %(contribDir)
  command += " -DMPI_CXX_COMPILER:FILEPATH=%s/openmpi/bin/mpicxx" %(contribDir)
  command += " -DMPI_Fortran_COMPILER:FILEPATH=%s/openmpi/bin/mpif90" %(contribDir)

if os.path.exists( contribDir + "/boost" ):
  command += " -DBOOST_ROOT:PATH=%s/boost" %(contribDir)
  command += " -DBoost_INCLUDE_DIRS:PATH=%s/boost/include" %(contribDir)
  command += " -DBoost_LIBRARY_DIRS:PATH=%s/boost/lib" %(contribDir)

if os.path.exists( contribDir + "/lapack" ):
  command += " -DBLAS_LIBRARIES:STRING=\"%s/lapack/lib/libblas.so\"" %(contribDir)
  command += " -DLAPACK_LIBRARIES:STRING=\"%s/lapack/lib/liblapack.so;%s/lapack/lib/libblas.so\"" %(contribDir,contribDir)

if os.path.exists( contribDir + "/hdf5" ):
  os.environ['HDF5_ROOT'] = "%s/hdf5" %(contribDir)

if os.path.exists( contribDir + "/cgns" ):
  command += " -DCGNS_ROOT:PATH=%s/cgns" %(contribDir)

if os.path.exists( contribDir + "/tecio" ):
  command += " -DTECIO_ROOT:PATH=%s/tecio" %(contribDir)

if os.path.exists( contribDir + "/scotch" ):
  command += " -DSCOTCH_ROOT:PATH=%s/scotch" %(contribDir)

if ( build_GUI.lower() == "true" ):
  if os.path.exists( contribDir + "/jpeg" ):
    command += " -DJPEG_ROOT:PATH=%s/jpeg" %(contribDir)

  if os.path.exists( contribDir + "/png" ):
    command += " -DPNG_ROOT:PATH=%s/png" %(contribDir)

  if os.path.exists( contribDir + "/freetype" ):
    command += " -DFREETYPE_ROOT:PATH=%s/freetype" %(contribDir)

  if os.path.exists( contribDir + "/qt5" ):
    command += " -DQT5_ROOT:PATH=%s/qt5" %(contribDir)

  if os.path.exists( contribDir + "/osg" ):
    command += " -DOSG_DIR:PATH=%s/osg" %(contribDir)

# if we are using Intel MPI, we need to supress a DEFINE in an MPI include.
if ( compiler.lower().find("intel") >= 0 ):
  command = command + " -DCMAKE_CXX_FLAGS:STRING=\"-DMPICH_IGNORE_CXX_SEEK\""


comdlog.write( "\n\n command: %s\n" %(command) )
comdlog.close()


cmdstatus = call( command, shell=True, stdout=conflog, stderr=conflog )

conflog.close()


# if something went wrong with cmake, let the user know about it.

if ( cmdstatus == 0 ):
  print( "\nCMake has configured the project properly. Go to:" )
  print( buildDir )
  print( "to compile (make) your project. Have a nice day ;)" )
elif ( cmdstatus != 0 ):
  conflog = open( buildDir + "/../codeFusion_configure.log", 'r')
  for data in conflog.readlines():
    sys.stdout.write( data )
  print( ( "log written to %s") %(buildDir + "/../codeFusion_configure.log") )
  conflog.close()


# ---------------------------------------------------------
# Normal script termination

endTime = time.time()
elapsed = endTime - startTime

if ( elapsed < 60 ):
   print("\nElapsed time [sec] = %f\n" %elapsed )
else:
   print("\nElapsed time [min] = %f\n" %(elapsed/60.0) )


sys.exit()

# ---------------------------------------------------------
