set ( OS_LINUX   false )
set ( OS_WINDOWS false )
set ( OS_MACOSX  false )

if ( ${CMAKE_SYSTEM_NAME} MATCHES "Linux"   )
set( OS_LINUX   true )
endif()

if ( ${CMAKE_SYSTEM_NAME} MATCHES "Windows" )
set( OS_WINDOWS true )
endif()

# Determine if we are on a 32 or 64 bit OS
set(OS_BITS 32)
if(CMAKE_SIZEOF_VOID_P EQUAL 8)
  set(OS_BITS 64)
endif()