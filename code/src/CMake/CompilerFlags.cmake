# -------------------------------------------------------------
# Linux Build system

if ( ${OS_LINUX} )

if ( CMAKE_COMPILER_IS_GNUCXX )
  set ( CMAKE_CXX_FLAGS_DEBUG   "${CMAKE_CXX_FLAGS} -g -O0 -Wall -std=c++11 -pedantic"  )
  set ( CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -O2 -std=c++11 -pedantic" )
  set ( CMAKE_EXE_LINKER_FLAGS  "-s" )

  # supress g++ annoying errors. Most come from Boost...
  set ( CMAKE_CXX_FLAGS_DEBUG   "${CMAKE_CXX_FLAGS_DEBUG} -Wno-long-long -Wno-unused-local-typedefs -Wno-unused-variable -Wno-return-type" )
endif ()

if ( ${CMAKE_CXX_COMPILER} MATCHES icpc )
   set ( CMAKE_CXX_FLAGS_DEBUG   "${CMAKE_CXX_FLAGS} -g -O0 -Wall -std=c++0x" )
   set ( CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -O2 -std=c++0x" )
  set ( CMAKE_EXE_LINKER_FLAGS  "-s" )
endif()

if ( ${BUILD_WITH_MPI} )
  set ( CMAKE_CXX_COMPILER ${MPI_CXX_COMPILER}         )
  set ( CMAKE_C_COMPILER   ${MPI_C_COMPILER}           )
  set ( CMAKE_Fortran_COMPILER ${MPI_Fortran_COMPILER} )
endif()

endif ()

# -------------------------------------------------------------
# Windows Build system

if ( ${OS_WINDOWS} )

if (MSVC)
  set ( CMAKE_CXX_FLAGS "")
  set ( CMAKE_CXX_FLAGS_DEBUG "/Debug /Zi")
  set ( CMAKE_CXX_FLAGS_RELEASE  "/O2" )
  set ( CMAKE_EXE_LINKER_FLAGS "" )
endif ()

if (MINGW)
  set ( CMAKE_CXX_FLAGS "-O2 -std=c++11")
  set ( CMAKE_CXX_FLAGS_DEBUG "-g -O0 -std=c++11 -Wall")
  set ( CMAKE_CXX_FLAGS_RELEASE  "-O2 -std=c++11 -w" )
  set ( CMAKE_EXE_LINKER_FLAGS "-s" )
endif ()

endif()
