# -------------------------------------------------------------
# Define install paths

set( INSTALL_ROOT_DIR
  ${CMAKE_INSTALL_PREFIX}
  CACHE STRING "Installation root directory path" )

set( INSTALL_BIN_DIR
  ${INSTALL_ROOT_DIR}/bin
  CACHE STRING "Installation path for application binaries" )

set( INSTALL_LIB_DIR
  ${INSTALL_ROOT_DIR}/lib
  CACHE STRING "Installation path for libraries" )

set( INSTALL_INCLUDE_DIR
  ${INSTALL_ROOT_DIR}/include
  CACHE STRING "Installation path for API header files" )

set( INSTALL_SHARE_DIR
  ${INSTALL_ROOT_DIR}/share
  CACHE STRING "Installation path for shared files" )

set( INSTALL_ARCHIVE_DIR
  ${INSTALL_ROOT_DIR}/archive
  CACHE STRING "Installation path for archive files" )

# define external libraries source directory
set( SOURCE_CONTRIB_DIR
  ${TPL_DIR}
  CACHE STRING "Location path of Third Party Libraries tarballs" )

# define external libraries install directory
set( INSTALL_CONTRIB_DIR
  ${INSTALL_ROOT_DIR}/contrib
  CACHE STRING "Installation path for Third Party Libraries (TPL)" )

MARK_AS_ADVANCED(
  INSTALL_ROOT_DIR
  INSTALL_BIN_DIR
  INSTALL_LIB_DIR
  INSTALL_INCLUDE_DIR
  INSTALL_SHARE_DIR
  INSTALL_ARCHIVE_DIR
)

# -------------------------------------------------------------
# Define include directories

include_directories( ${INSTALL_INCLUDE_DIR} ${INSTALL_LIB_DIR} )

set( INSTALL_EXTERNAL_INCLUDE ${INSTALL_EXTERNAL_DIR}/include )

include_directories( ${INSTALL_EXTERNAL_INCLUDE} )
