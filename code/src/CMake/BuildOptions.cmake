# -------------------------------------------------------------
# Global build options

# Build with MPI support
option( BUILD_WITH_MPI "Enable MPI support" ON )

# Build GUI
option( BUILD_GUI "Enable GUI building" OFF )

# Build test applications
option( BUILD_TESTS "Build test applications" ON )

# Define Real type precision (SINGLE/DOUBLE/QUAD)
set( REAL_PRECISION "DOUBLE" CACHE STRING "SINGLE/DOUBLE/QUAD" )

# Default behaviour is to perform a DEBUG building
set( ${CMAKE_BUILD_TYPE} "Debug")

# -------------------------------------------------------------
# process real type precision option

set( REAL_IS_FLOAT      false )
set( REAL_IS_DOUBLE     false )
set( REAL_IS_LONGDOUBLE false )

if ( REAL_PRECISION MATCHES "[Ss][Ii][Nn][Gg][Ll][Ee]" )
  set( REAL_IS_FLOAT true )
endif()

if ( REAL_PRECISION MATCHES "[Dd][Oo][Uu][Bb][Ll][Ee]" )
  set( REAL_IS_DOUBLE true )
endif()

if ( REAL_PRECISION MATCHES "[Qq][Uu][Aa][Dd]" )
  set( REAL_IS_LONGDOUBLE true )
endif()
