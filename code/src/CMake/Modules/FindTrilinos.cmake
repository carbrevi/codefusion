#
# Find the native Trilinos includes and library
#
# Trilinos_INCLUDE_DIRS - where to find headers
# Trilinos_LIBRARIES    - List of fully qualified libraries to link against when using Trilinos.
# Trilinos_FOUND        - Do not attempt to use Trilinos if "no" or undefined.

set(Trilinos_DIR "${INSTALL_CONTRIB_DIR}/trilinos")

IF(EXISTS "${Trilinos_DIR}" AND IS_DIRECTORY "${Trilinos_DIR}")

  include("${Trilinos_DIR}/lib/cmake/Trilinos/TrilinosConfig.cmake")

  SET( Trilinos_FOUND "NO" )
  IF(Trilinos_INCLUDE_DIRS)
    IF(Trilinos_LIBRARIES)
      SET( Trilinos_FOUND "YES" )
    ENDIF(Trilinos_LIBRARIES)
  ENDIF(Trilinos_INCLUDE_DIRS)

ENDIF()

IF(Trilinos_FIND_REQUIRED AND NOT Trilinos_FOUND)
  message(SEND_ERROR "Unable to find the requested Trilinos libraries.")
ENDIF(Trilinos_FIND_REQUIRED AND NOT Trilinos_FOUND)

# handle the QUIETLY and REQUIRED arguments and set Trilinos_FOUND to TRUE if 
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Trilinos DEFAULT_MSG Trilinos_LIBRARIES Trilinos_INCLUDE_DIRS)

MARK_AS_ADVANCED(
  Trilinos_INCLUDE_DIRS
  Trilinos_LIBRARIES
)