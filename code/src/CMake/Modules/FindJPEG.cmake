#
# Find the native JPEG includes and library
#
# JPEG_INCLUDE_DIRS - where to find header, etc.
# JPEG_LIBRARIES      - List of fully qualified libraries to link against when using JPEG.
# JPEG_FOUND        - Do not attempt to use JPEG if "no" or undefined.

FIND_PATH(JPEG_INCLUDE_DIRS jpeglib.h NO_DEFAULT_PATH PATHS
  ${JPEG_ROOT}/include
)

FIND_LIBRARY(JPEG_LIBRARIES jpeg NO_DEFAULT_PATH PATHS
  ${JPEG_ROOT}/lib
)

SET( JPEG_FOUND "NO" )
IF(JPEG_INCLUDE_DIRS)
  IF(JPEG_LIBRARIES)
    SET( JPEG_FOUND "YES" )
  ENDIF(JPEG_LIBRARIES)
ENDIF(JPEG_INCLUDE_DIRS)

IF(JPEG_FIND_REQUIRED AND NOT JPEG_FOUND)
  message(SEND_ERROR "Unable to find the requested JPEG libraries.")
ENDIF(JPEG_FIND_REQUIRED AND NOT JPEG_FOUND)

# handle the QUIETLY and REQUIRED arguments and set HDF5_FOUND to TRUE if 
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(JPEG DEFAULT_MSG JPEG_LIBRARIES JPEG_INCLUDE_DIRS)

MARK_AS_ADVANCED(
  JPEG_INCLUDE_DIRS
  JPEG_LIBRARIES
)
