#
# Find the native OCE (Opencascade Community Edition) includes and library
#
# OCE_INCLUDE_DIRS - where to find oce-config.h, etc.
# OCE_LIBRARIES   - List of fully qualified libraries to link against when using OCE.
# OCE_FOUND       - Do not attempt to use OCE if "no" or undefined.

set(OCE_DIR "${INSTALL_CONTRIB_DIR}/oce")

IF(EXISTS "${OCE_DIR}" AND IS_DIRECTORY "${OCE_DIR}")

  include("${OCE_DIR}/lib/oce-0.14/OCEConfig.cmake")

  SET( OCE_FOUND "NO" )
  IF(OCE_INCLUDE_DIRS)
    IF(OCE_LIBRARIES)
      SET( OCE_FOUND "YES" )
    ENDIF(OCE_LIBRARIES)
  ENDIF(OCE_INCLUDE_DIRS)

ENDIF()


IF(OCE_FIND_REQUIRED AND NOT OCE_FOUND)
  message(SEND_ERROR "Unable to find the requested OCE libraries.")
ENDIF(OCE_FIND_REQUIRED AND NOT OCE_FOUND)

# handle the QUIETLY and REQUIRED arguments and set OCE_FOUND to TRUE if 
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(OCE DEFAULT_MSG OCE_LIBRARIES OCE_INCLUDE_DIRS)


MARK_AS_ADVANCED(
  OCE_INCLUDE_DIRS
  OCE_LIBRARIES
)
