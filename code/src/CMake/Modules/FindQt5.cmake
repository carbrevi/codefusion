#
# Find the native Qt5 includes and library. This is just a wrapper
# to detect if Qt5 is installed in the system. Given that its design
# is highly modular, the application will have to define the sub-packages
# it will use and look for the appropriate libraries then.
# Qt5_ROOT should provide enough hints to look for the components.
#
# Qt5_INCLUDE_DIRS - where to find headers
# Qt5_LIBRARIES    - List of fully qualified libraries to link against.
# Qt5_FOUND        - Do not attempt to use Qt5 if "no" or undefined.

IF(EXISTS "${QT5_ROOT}" AND IS_DIRECTORY "${QT5_ROOT}")

  include("${QT5_ROOT}/lib/cmake/Qt5Core/Qt5CoreConfig.cmake")

  SET( Qt5_FOUND "YES" )
   IF(Qt5Core_INCLUDE_DIRS)
     IF(Qt5Core_LIBRARIES)
       SET( Qt5_INCLUDE_DIRS "${QT5_ROOT}/include" )
       SET( Qt5_LIBRARIES "${QT5_ROOT}/lib" )
       SET( Qt5_FOUND "YES" )
     ENDIF(Qt5Core_LIBRARIES)
   ENDIF(Qt5Core_INCLUDE_DIRS)

ENDIF()

IF(Qt5_FIND_REQUIRED AND NOT Qt5_FOUND)
  message(SEND_ERROR "Unable to find the requested Qt5 libraries.")
ENDIF(Qt5_FIND_REQUIRED AND NOT Qt5_FOUND)

# handle the QUIETLY and REQUIRED arguments and set Qt5_FOUND to TRUE if 
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Qt5 DEFAULT_MSG Qt5_LIBRARIES Qt5_INCLUDE_DIRS)

MARK_AS_ADVANCED(
  Qt5_INCLUDE_DIRS
  Qt5_LIBRARIES
)
