#
# Find the native TECIO includes and library
#
# TECIO_INCLUDE_DIR - where to find TECIO.h, etc.
# TECIO_LIBRARIES   - List of fully qualified libraries to link against when using TECIO.
# TECIO_FOUND       - Do not attempt to use TECIO if "no" or undefined.

FIND_PATH(TECIO_INCLUDE_DIR TECIO.h
  ${TECIO_ROOT}/include
)

FIND_LIBRARY(TECIO_LIBRARY tecio
  ${TECIO_ROOT}/lib
)

SET( TECIO_FOUND "NO" )
IF(TECIO_INCLUDE_DIR)
  IF(TECIO_LIBRARY)
    SET( TECIO_LIBRARIES ${TECIO_LIBRARY} )
    SET( TECIO_FOUND "YES" )
  ENDIF(TECIO_LIBRARY)
ENDIF(TECIO_INCLUDE_DIR)

IF(TECIO_FIND_REQUIRED AND NOT TECIO_FOUND)
  message(SEND_ERROR "Unable to find the requested TECIO libraries.")
ENDIF(TECIO_FIND_REQUIRED AND NOT TECIO_FOUND)

# handle the QUIETLY and REQUIRED arguments and set TECIO_FOUND to TRUE if 
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(TECIO DEFAULT_MSG TECIO_LIBRARY TECIO_INCLUDE_DIR)


MARK_AS_ADVANCED(
  TECIO_INCLUDE_DIR
  TECIO_LIBRARY
)
