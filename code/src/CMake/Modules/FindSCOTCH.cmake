#
# Find the native SCOTCH includes and library
#
# SCOTCH_INCLUDE_DIR - where to find scotch.h, etc.
# SCOTCH_LIBRARIES   - List of fully qualified libraries to link against when using SCOTCH.
# SCOTCH_FOUND       - Do not attempt to use SCOTCH if "no" or undefined.

FIND_PATH(SCOTCH_INCLUDE_DIR scotch.h
  ${SCOTCH_ROOT}/include
)

FIND_LIBRARY(SCOTCH_LIBRARY scotch
  ${SCOTCH_ROOT}/lib
)

SET( SCOTCH_FOUND "NO" )
IF(SCOTCH_INCLUDE_DIR)
  IF(SCOTCH_LIBRARY)
    SET( SCOTCH_LIBRARIES ${SCOTCH_LIBRARY} )
    SET( SCOTCH_FOUND "YES" )
  ENDIF(SCOTCH_LIBRARY)
ENDIF(SCOTCH_INCLUDE_DIR)

IF(SCOTCH_FIND_REQUIRED AND NOT SCOTCH_FOUND)
  message(SEND_ERROR "Unable to find the requested SCOTCH libraries.")
ENDIF(SCOTCH_FIND_REQUIRED AND NOT SCOTCH_FOUND)

# handle the QUIETLY and REQUIRED arguments and set SCOTCH_FOUND to TRUE if 
# all listed variables are TRUE
# INCLUDE(FindPackageHandleStandardArgs)
# FIND_PACKAGE_HANDLE_STANDARD_ARGS(SCOTCH DEFAULT_MSG SCOTCH_LIBRARY SCOTCH_INCLUDE_DIR)


MARK_AS_ADVANCED(
  SCOTCH_INCLUDE_DIR
  SCOTCH_LIBRARY
)
