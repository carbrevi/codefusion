#
# Find the PIXZ executable
#
# PIXZ_EXEC - where to find pixz executable, etc.
# PIXZ_FOUND - Do not attempt to use PIXZ if "no" or undefined.

FIND_FILE( PIXZ_EXEC NAMES "pixz" PATHS
  ${PIXZ_ROOT}/bin
  NO_DEFAULT_PATH
)

SET( PIXZ_FOUND "NO" )
IF(PIXZ_EXEC)
  SET( PIXZ_FOUND "YES" )
ENDIF(PIXZ_EXEC)

IF(PIXZ_FIND_REQUIRED AND NOT PIXZ_FOUND)
  message(SEND_ERROR "Unable to find the requested PIXZ executable.")
ENDIF(PIXZ_FIND_REQUIRED AND NOT PIXZ_FOUND)

# handle the QUIETLY and REQUIRED arguments and set PIXZ_FOUND to TRUE if 
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PIXZ DEFAULT_MSG PIXZ_EXEC)

