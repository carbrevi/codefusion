#
# Find the native PNG includes and library
#
# PNG_INCLUDE_DIRS - where to find header, etc.
# PNG_LIBRARIES      - List of fully qualified libraries to link against when using PNG.
# PNG_FOUND        - Do not attempt to use PNG if "no" or undefined.

FIND_PATH(PNG_INCLUDE_DIRS png.h NO_DEFAULT_PATH PATHS
  ${PNG_ROOT}/include
)

FIND_LIBRARY(PNG_LIBRARIES png NO_DEFAULT_PATH PATHS
  ${PNG_ROOT}/lib
)

SET( PNG_FOUND "NO" )
IF(PNG_INCLUDE_DIRS)
  IF(PNG_LIBRARIES)
    SET( PNG_FOUND "YES" )
  ENDIF(PNG_LIBRARIES)
ENDIF(PNG_INCLUDE_DIRS)

IF(PNG_FIND_REQUIRED AND NOT PNG_FOUND)
  message(SEND_ERROR "Unable to find the requested PNG libraries.")
ENDIF(PNG_FIND_REQUIRED AND NOT PNG_FOUND)

# handle the QUIETLY and REQUIRED arguments and set HDF5_FOUND to TRUE if 
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PNG DEFAULT_MSG PNG_LIBRARIES PNG_INCLUDE_DIRS)

MARK_AS_ADVANCED(
  PNG_INCLUDE_DIRS
  PNG_LIBRARIES
)
