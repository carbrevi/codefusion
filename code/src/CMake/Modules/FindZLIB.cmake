#
# Find the ZLIB includes and library
#
# ZLIB_INCLUDE_DIRS - where to find zlib.h, etc.
# ZLIB_LIBRARIES    - List of fully qualified libraries to link against.
# ZLIB_FOUND        - Do not attempt to use ZLIB if "no" or undefined.

FIND_PATH(ZLIB_INCLUDE_DIRS "zlib.h" NO_DEFAULT_PATH PATHS
  ${ZLIB_ROOT}/include
)

FIND_LIBRARY(ZLIB_LIBRARIES "z" NO_DEFAULT_PATH PATHS
  ${ZLIB_ROOT}/lib
)

SET( ZLIB_FOUND "NO" )
IF(ZLIB_INCLUDE_DIRS)
  IF(ZLIB_LIBRARIES)
    SET( ZLIB_FOUND "YES" )
  ENDIF(ZLIB_LIBRARIES)
ENDIF(ZLIB_INCLUDE_DIRS)

IF(ZLIB_FIND_REQUIRED AND NOT ZLIB_FOUND)
  message(SEND_ERROR "Unable to find the requested ZLIB libraries.")
ENDIF(ZLIB_FIND_REQUIRED AND NOT ZLIB_FOUND)

# handle the QUIETLY and REQUIRED arguments and set HDF5_FOUND to TRUE if 
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(ZLIB DEFAULT_MSG ZLIB_LIBRARIES ZLIB_INCLUDE_DIRS)

MARK_AS_ADVANCED(
  ZLIB_INCLUDE_DIRS
  ZLIB_LIBRARIES
)
