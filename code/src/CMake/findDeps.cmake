# -------------------------------------------------------------
# CMake macro to find external projects
#

set( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/CMake/Modules" )

include(CheckIncludeFile)
include(CheckIncludeFiles)

# -------------------------------------------------------------
# Check if the required TPLs exist on the system

set(PACKAGES
PIXZ
MPI
ZLIB
Boost
TECIO
OCE
HDF5
CGNS
LAPACK
SCOTCH
Trilinos
)

if( ${BUILD_GUI} )
  set( PACKAGES ${PACKAGES} OpenGL)
  set( PACKAGES ${PACKAGES} JPEG)
  set( PACKAGES ${PACKAGES} PNG)
  set( PACKAGES ${PACKAGES} FREETYPE)
  set( PACKAGES ${PACKAGES} Qt5)
  set( PACKAGES ${PACKAGES} OSG)
endif()


# search for TPL and decide if we have to build it or not

find_package( Pthreads )

foreach(item ${PACKAGES})

  set(TPL_HAVE_${item} OFF)

  if ( ${item} MATCHES "Boost" )

    set( BOOST_MIN_VERSION "1.39.0" )
    find_package( Boost ${BOOST_MIN_VERSION} )
    if ( Boost_FOUND )
      set( TPL_HAVE_Boost ON )
    endif()

  elseif( ${item} MATCHES "HDF5" )

    find_package( ${item} )
    if ( HDF5_FOUND AND HDF5_IS_PARALLEL )
      set( TPL_HAVE_HDF5 ON )
    endif()

  elseif( ${item} MATCHES "OpenGL" )

    find_package( ${item} )
    if ( OPENGL_FOUND )
      set( TPL_HAVE_OpenGL ON )
    endif()

  elseif( ${item} MATCHES "OSG" )

    # Note that the components ordering here is importanto for static linking.
    find_package( OpenSceneGraph COMPONENTS
      osgViewer osgGA osgFX osgTerrain osgSim osgText osgDB osgUtil osgQt
    )
    if ( OPENSCENEGRAPH_FOUND )
      set( TPL_HAVE_OSG ON )
    endif()

  else()

    find_package( ${item} )

    if ( ${item}_FOUND )
      set( TPL_HAVE_${item} ON )
    endif()

  endif()

endforeach(item)

# -------------------------------------------------------------
# Create build instructions for TPLs that were not yet compiled

foreach(item ${PACKAGES})

  if ( NOT TPL_HAVE_${item} )

    string(TOLOWER ${item} namepkg)
    set( DEPS ${DEPS} ${namepkg} )

  endif()

endforeach(item)

# Write data for python TPLs build script

if ( DEPS )

  set( CONTRIB_INPUT_FILE ${CMAKE_BINARY_DIR}/../contrib.inp )

  file( WRITE ${CONTRIB_INPUT_FILE} "# System TPL data")

  # write list of compiler and MPI environment variables
  file(
    APPEND
    ${CONTRIB_INPUT_FILE}
    "\nCC=$ENV{CC}"
    "\nCXX=$ENV{CXX}"
    "\nFC=$ENV{FC}"
    "\nMPI_CC=$ENV{MPI_CC}"
    "\nMPI_CXX=$ENV{MPI_CXX}"
    "\nMPI_F90=$ENV{MPI_F90}"
  )

  # inform python of TPLs directory.
  file(
    APPEND
    ${CONTRIB_INPUT_FILE}
    "\nTPL_DIR=${SOURCE_CONTRIB_DIR}"
  )

  # inform python of install directory.
  file(
    APPEND
    ${CONTRIB_INPUT_FILE}
    "\ninstall_prefix=${INSTALL_CONTRIB_DIR}"
  )

  # write list of TPLs to build
  file(
    APPEND
    ${CONTRIB_INPUT_FILE}
    "\nTPL=${DEPS}"
  )

  # write list of available TPLs on the system
  foreach(item ${PACKAGES})
    file(
      APPEND
      ${CONTRIB_INPUT_FILE}
      "\nTPL_HAVE_${item}=${TPL_HAVE_${item}}"
    )
  endforeach(item)

  if ( PTHREADS_FOUND )
    file(
      APPEND
      ${CONTRIB_INPUT_FILE}
      "\nPTHREADS_INCLUDE_DIR=${PTHREADS_INCLUDE_DIR}"
      "\nPTHREADS_LIBRARY=${PTHREADS_LIBRARY}"
    )
  endif()

  if ( TPL_HAVE_MPI )
    file(
      APPEND
      ${CONTRIB_INPUT_FILE}
      "\nMPI_C_COMPILER=${MPI_C_COMPILER}"
      "\nMPI_CXX_COMPILER=${MPI_CXX_COMPILER}"
      "\nMPI_Fortran_COMPILER=${MPI_Fortran_COMPILER}"
      "\nMPIEXEC=${MPIEXEC}"
      "\nMPI_C_INCLUDE_PATH=${MPI_C_INCLUDE_PATH}"
      "\nMPI_CXX_INCLUDE_PATH=${MPI_CXX_INCLUDE_PATH}"
      "\nMPI_Fortran_INCLUDE_PATH=${MPI_Fortran_INCLUDE_PATH}"
      "\nMPI_C_LIBRARIES=${MPI_C_LIBRARIES}"
      "\nMPI_CXX_LIBRARIES=${MPI_CXX_LIBRARIES}"
      "\nMPI_Fortran_LIBRARIES=${MPI_Fortran_LIBRARIES}"

    )
  endif()


  # for the available packages in the system, list the proper
  # paths and link information.
  foreach(item ${PACKAGES})
    if ( TPL_HAVE_${item} )

      if( ${item} MATCHES "OpenGL" )
        file(
          APPEND
          ${CONTRIB_INPUT_FILE}
          "\n${item}_ROOT=${${item}_ROOT}"
          "\n${item}_INCLUDE_DIRS=${OPENGL_INCLUDE_DIR}"
          "\n${item}_LIBRARIES=${OPENGL_LIBRARIES}"
          "\n${item}_gl_LIBRARY=${OPENGL_gl_LIBRARY}"
          "\n${item}_glu_LIBRARY=${OPENGL_glu_LIBRARY}"
        )

      elseif( ${item} MATCHES "OSG" )
        file(
          APPEND
          ${CONTRIB_INPUT_FILE}
          "\n${item}_ROOT=${${item}_ROOT}"
          "\n${item}_INCLUDE_DIRS=${OPENSCENEGRAPH_INCLUDE_DIRS}"
          "\n${item}_LIBRARIES=${OPENSCENEGRAPH_LIBRARIES}"
        )

      elseif( ${item} MATCHES "PIXZ" )
        file(
          APPEND
          ${CONTRIB_INPUT_FILE}
          "\n${item}_ROOT=${${item}_ROOT}"
          "\n${item}_EXECUTABLE=${PIXZ_EXEC}"
        )

      else()

        file(
          APPEND
          ${CONTRIB_INPUT_FILE}
          "\n${item}_ROOT=${${item}_ROOT}"
          "\n${item}_INCLUDE_DIRS=${${item}_INCLUDE_DIRS}"
          "\n${item}_LIBRARIES=${${item}_LIBRARIES}"
        )

      endif()

    endif()
  endforeach(item)

  # tell the user which libraries need to be built and terminate the configure process.
  set( build_script "${CMAKE_SOURCE_DIR}/../contrib/build.py" )
  message(
    FATAL_ERROR
    "\n***************************************************************"
    "\nThe following dependencies need to be built before continuing:"
    "\n${DEPS}"
    "\nExecute the python script:"
    "\n${build_script}"
    "\nto compile the missing libraries. Run this script again later."
    "\n***************************************************************\n"
  )

endif()

# -------------------------------------------------------------
