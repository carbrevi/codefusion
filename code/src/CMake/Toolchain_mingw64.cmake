# the name of the target operating system
SET(CMAKE_SYSTEM_NAME Windows)

# Choose an appropriate compiler prefix

# for classical mingw32
# see http://www.mingw.org/
#set(COMPILER_PREFIX "i586-mingw32msvc")

# for 32 or 64 bits mingw-w64
# see http://mingw-w64.sourceforge.net/
#set(COMPILER_PREFIX "i686-w64-mingw32")
set(COMPILER_PREFIX "/home/carbrevi/opt/mingw64/bin/x86_64-w64-mingw32")
SET(CMAKE_RC_COMPILER		${COMPILER_PREFIX}-windres)
SET(CMAKE_C_COMPILER		${COMPILER_PREFIX}-gcc)
SET(CMAKE_CXX_COMPILER 		${COMPILER_PREFIX}-g++)
SET(CMAKE_Fortran_COMPILER	${COMPILER_PREFIX}-gfortran)
SET(CMAKE_AR				${COMPILER_PREFIX}-ar)
SET(CMAKE_LD				${COMPILER_PREFIX}-ld)
SET(CMAKE_NM				${COMPILER_PREFIX}-nm)
SET(CMAKE_OBJCOPY			${COMPILER_PREFIX}-objcopy)
SET(CMAKE_OBJDUMP			${COMPILER_PREFIX}-objdump)
SET(CMAKE_RANLIB			${COMPILER_PREFIX}-ranlib)
SET(CMAKE_STRIP				${COMPILER_PREFIX}-strip)
SET(CMAKE_MC				${COMPILER_PREFIX}-windmc)


# SET(CMAKE_LINKER ${COMPILER_PREFIX}-ld)

# which compilers to use for C and C++
#find_program(CMAKE_RC_COMPILER NAMES ${COMPILER_PREFIX}-windres)
#SET(CMAKE_RC_COMPILER ${COMPILER_PREFIX}-windres)
#find_program(CMAKE_C_COMPILER NAMES ${COMPILER_PREFIX}-gcc)
#SET(CMAKE_C_COMPILER ${COMPILER_PREFIX}-gcc)
#find_program(CMAKE_CXX_COMPILER NAMES ${COMPILER_PREFIX}-g++)
#SET(CMAKE_CXX_COMPILER ${COMPILER_PREFIX}-g++)


# here is the target environment located
SET(USER_ROOT_PATH /home/carbrevi/opt/mingw64/x86_64-w64-mingw32/ )
SET(CMAKE_FIND_ROOT_PATH  ${USER_ROOT_PATH})

# adjust the default behaviour of the FIND_XXX() commands:
# search headers and libraries in the target environment, search 
# programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)