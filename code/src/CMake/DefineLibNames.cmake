# -------------------------------------------------------------
set(fusion_kernel_LIBRARY "fusionkernel")
set(fusion_geometry_LIBRARY "fusiongeometry")
set(fusion_localelements_LIBRARY "fusionlocalelements")
set(fusion_grid_LIBRARY "fusiongrid")
set(fusion_functionspace_LIBRARY "fusionfunctionspace")
set(fusion_discretizations_LIBRARY "fusiondiscretizations")
set(fusion_linearsolvers_LIBRARY "fusionlinearsolvers")
set(fusion_nonlinearsolvers_LIBRARY "fusionnonlinearsolvers")