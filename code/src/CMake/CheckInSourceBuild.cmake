# prohibit in-source build
if( "${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_BINARY_DIR}" )
  message( FATAL_ERROR "${PROJECT_NAME} requires an out of source build." )
endif()