# order does matter
add_subdirectory( kernel )
add_subdirectory( geometry )
add_subdirectory( local_elements )
add_subdirectory( grid )
#
add_subdirectory( function_spaces )
add_subdirectory( discretizations )
#
add_subdirectory( linear_solvers )
add_subdirectory( nonlinear_solvers )