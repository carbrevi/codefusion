#include "fusion.hpp"

namespace codeFusion
{

fusion::fusion(int argc, char** argv)
{
  argc_ = argc;
  argv_ = argv;

  // Setup timer for the whole application
  Teuchos::RCP< Teuchos::Time > total_time =
    Teuchos::TimeMonitor::getNewTimer("Total time");
  Teuchos::TimeMonitor timer(*total_time);

  // Teuchos::GlobalMPISession calls MPI_Finilize() in its destructor.
  Teuchos::oblackholestream blackhole;
  mpiSession_ = Teuchos::rcp(new Teuchos::GlobalMPISession(&argc_, &argv_, &blackhole));
  comm_ = Teuchos::DefaultComm<int>::getComm();

  outStream_ = Teuchos::rcp(new Teuchos::FancyOStream(Teuchos::rcp(&std::cout, false)));

  if (mpiSession_->getNProc() > 1)
  {
    outStream_->setShowProcRank(true);
    outStream_->setOutputToRootOnly(0);
  }

  // create input parameter object
  inputList_ = Teuchos::rcp(new paramList(argc_,argv_,outStream_,comm_));

}

}
