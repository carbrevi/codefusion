#ifndef FUSION_INPUTPARAMLIST_HEADER
#define FUSION_INPUTPARAMLIST_HEADER

#include <Teuchos_ParameterList.hpp>
#include <Teuchos_CommandLineProcessor.hpp>
#include <Teuchos_XMLParameterListHelpers.hpp>


namespace codeFusion
{

class paramList
{
  public:
    paramList(int argc, char** argv,
              const Teuchos::RCP<Teuchos::FancyOStream> out,
              Teuchos::RCP<const Teuchos::Comm<int> > comm):
      argc_(argc), argv_(argv),
      outStream_(out), comm_(comm) {}

    void read_and_broadcast();

    void report_input_params();

    Teuchos::RCP<Teuchos::ParameterList> getInputParamList()
    {
      return input_params_;
    }

  private:
    int argc_;
    char** argv_;
    std::string filename_;
    Teuchos::RCP<Teuchos::ParameterList> input_params_;
    Teuchos::RCP<Teuchos::FancyOStream> outStream_;
    Teuchos::RCP<const Teuchos::Comm<int> > comm_;

};

}

#endif // FUSION_INPUTPARAMLIST_HEADER
