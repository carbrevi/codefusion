#include "input_paramlist.hpp"

namespace codeFusion
{

void paramList::read_and_broadcast()
{

  // Parse command line parameters to get filename
  // ///////////////////////////////////////////////////

  Teuchos::RCP<Teuchos::CommandLineProcessor> commandLine =
    Teuchos::rcp(new Teuchos::CommandLineProcessor);

  commandLine->setOption("input", &filename_, "xml input filename");

  Teuchos::CommandLineProcessor::EParseCommandLineReturn parse_return =
    commandLine->parse(argc_, argv_, &std::cerr);

  TEUCHOS_TEST_FOR_EXCEPTION(parse_return !=
                             Teuchos::CommandLineProcessor::PARSE_SUCCESSFUL,
                             std::runtime_error, "Failed to parse command line!");

  // Parse input file and broadcast to other procs
  // ///////////////////////////////////////////////////

  input_params_ = Teuchos::rcp(new Teuchos::ParameterList("app Parameters"));

  Teuchos::updateParametersFromXmlFileAndBroadcast(filename_, input_params_.ptr(), *comm_);

}

void paramList::report_input_params()
{
  *outStream_ << *input_params_ << std::endl;
}

}
