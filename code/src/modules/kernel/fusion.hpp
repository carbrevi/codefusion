#ifndef FUSION_COMMON_HEADER
#define FUSION_COMMON_HEADER

#include "cf_config.hpp"
#include "utils/rcp.hpp"
#include "utils/timer.hpp"
#include "PE/PE.hpp"
#include "dataIO/input_paramlist.hpp"
#include "dataIO/output_stream.hpp"

namespace codeFusion
{

// Definition of the default precision
# ifdef REAL_IS_FLOAT
typedef float Real;
# endif
# ifdef REAL_IS_DOUBLE
typedef double Real;
# endif
# ifdef REAL_IS_LONGDOUBLE
typedef long double Real;
# endif

// Helper macros
#define printdbg( message ) std::cout << message << std::endl;

class fusion
{
  public:
    fusion(int argc, char** argv);

    Teuchos::RCP<Teuchos::FancyOStream> getOutStream()
    {
      return outStream_;
    }

    Teuchos::RCP<paramList> getInput(){
        return inputList_;
    }


  private:
    int argc_;
    char** argv_;

    Teuchos::RCP<Teuchos::FancyOStream> outStream_;

    Teuchos::RCP<Teuchos::GlobalMPISession> mpiSession_;
    Teuchos::RCP<const Teuchos::Comm<int> > comm_;

    Teuchos::RCP<paramList> inputList_;

};

}

#endif // FUSION_COMMON_HEADER
