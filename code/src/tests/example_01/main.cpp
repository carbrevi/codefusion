/* Carlos Breviglieri
 * November, 2013
 *
 * This example is created to sample the kernel library.
 *
 * The following steps are performed:
 * - read input file and spread over MPI communicator;
 * - create simple mesh object;
 * - partition and distribute the mesh over the communicator;
 * - assign processor id to mesh elements;
 * - write data to a tecplot binary file;
 *
*/

#include <fusion.hpp>
#include <grid.hpp>
using codeFusion::fusion;

// ------------------------------------------------------------

int main(int argc, char** argv)
{

  int status = 0;

  // initialize the library
  Teuchos::RCP<fusion> libFusion =
    Teuchos::rcp(new fusion(argc, argv));

  // grab output stream of parallel environment
  Teuchos::RCP<Teuchos::FancyOStream> out = libFusion->getOutStream();

  try
  {

    // read input file and broadcast
    libFusion->getInput()->read_and_broadcast();
    libFusion->getInput()->report_input_params();

    // create mesh object


  }
  catch (std::exception& e)
  {
    *out << "************* Caught Exception: Begin Error Report *************" << std::endl;
    *out << e.what() << std::endl;
    *out << "************* Caught Exception: End Error Report *************" << std::endl;
    status = -1;
  }
  catch (std::string& msg)
  {
    *out << "************* Caught Exception: Begin Error Report *************" << std::endl;
    *out << msg << std::endl;
    *out << "************* Caught Exception: End Error Report *************" << std::endl;
    status = -1;
  }
  catch (...)
  {
    *out << "************* Caught Exception: Begin Error Report *************" << std::endl;
    *out << "Caught UNKNOWN exception" << std::endl;
    *out << "************* Caught Exception: End Error Report *************" << std::endl;
    status = -1;
  }

  // Finish execution
  // ///////////////////////////////////////////////////

  Teuchos::TimeMonitor::summarize(*out, false, true, false);

  if (status == 0) *out << "codeFusion::example01 run completed" << std::endl;

  return status;

}

// ------------------------------------------------------------
