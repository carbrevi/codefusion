#!/usr/bin/python -B
#
# Invoking python with the B switch turns off bytecode
# generation in the source tree.
#
# This code pushes the current directory modifications to
# the remote git repository.
#
# Remote repository:
# https://carbrevi@bitbucket.org/carbrevi/codefusion.git


import sys
from subprocess import call

# get python version
if sys.version_info < (3, 0):
  python_ver = 2
elif sys.version_info >= (3, 0):
  python_ver = 3

# commit to remote repository
command = "git add --all *"
call( command, shell=True )

# get commit message from command line
print("Enter your commit message (ctrl-d to quit):")
lines = sys.stdin.readlines()
message = ''.join(lines)

command = "git commit -m $\'%s\'"%message
call( command, shell=True )

# show modifications to user
command = "git status"
call( command, shell=True )

# push modifications to remote repository
print()
if python_ver == 2:
  pushok = raw_input("Push modifications? [Y/n]: ")
elif python_ver == 3:
  pushok = input("Push modifications? [Y/n]: ")

pushtorepo = True # default action
if ( (pushok == "Y") or (pushok == "y") ): pushtorepo = True
if ( (pushok == "N") or (pushok == "n") ): pushtorepo = False

if pushtorepo:
  command = "git push -u origin master"
  call( command, shell=True )

## update local repository
#git pull origin master
